//
//  ViewController.h
//  AlarmaJtech
//
//  Created by Javier Aznar on 12/02/13.
//  Copyright (c) 2013 jtech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *segundosTextField;
@property (weak, nonatomic) IBOutlet UITextField *textoAlarma;
- (IBAction)tapButton:(id)sender;
- (IBAction)tapCancelarAlarmas:(id)sender;

@end
