//
//  ViewController.m
//  AlarmaJtech
//
//  Copyright (c) 2013 jtech. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tapButton:(id)sender {
    
    
    if (self.textoAlarma.text != nil && ![self.textoAlarma.text isEqualToString:@""] &&
        self.segundosTextField.text != nil && ![self.segundosTextField.text isEqualToString:@""])
    {
        int segundosParaSonar = [self.segundosTextField.text intValue];
    
        NSDate *date = [[NSDate date]dateByAddingTimeInterval:segundosParaSonar];
        // TODO: Programar notificación local con el texto del campo textoAlarma y los segundos que se
        // indican en el campo segundosTextField
       
        UIApplication* app = [UIApplication sharedApplication];
        
        UILocalNotification* alarm = [[UILocalNotification alloc] init];
        if (alarm)
        {
            
            alarm.fireDate = date;
            alarm.timeZone = [NSTimeZone defaultTimeZone];
            alarm.soundName = @"alarmsound.caf";
            alarm.alertBody = self.textoAlarma.text;
            
            [app scheduleLocalNotification:alarm];
        }
        
        
        
        
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alarma"
                                                        message:@"Alarma activada con éxito"
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Ok", nil];
        
        [alert show];
        
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alarma"
                                                        message:@"ERROR al programar la alarma. Revisa los campos"
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Ok", nil];
        
        [alert show];
    }
    
}

- (IBAction)tapCancelarAlarmas:(id)sender {
    // TODO: Implementar método
    
    UIApplication* app = [UIApplication sharedApplication];
    
    NSArray *notificacionesProgramadas = [app scheduledLocalNotifications];
    
    if ([notificacionesProgramadas count] > 0)
        [app cancelAllLocalNotifications];
    
}
@end
