//
//  UAAppDelegate.h
//  ProcesamientoImagen
//
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UAViewController;

@interface UAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UAViewController *viewController;

@end
