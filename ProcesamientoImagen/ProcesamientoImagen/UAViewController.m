//
//  UAViewController.m
//  ProcesamientoImagen
//
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UAViewController.h"

@implementation UAViewController

@synthesize imageView = _imageView;
@synthesize glkView = _glkView;

@synthesize imagenOriginal = _imagenOriginal;
@synthesize imagenFiltrada = _imagenFiltrada;

@synthesize contextCPU = _contextCPU;
@synthesize contextGPU = _contextGPU;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.imagenOriginal = [CIImage imageWithContentsOfURL:[[NSBundle mainBundle] URLForResource:@"foto" withExtension:@"jpg"]];
    
    EAGLContext *glContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    self.glkView.context = glContext;
    self.glkView.delegate = self;

    self.contextGPU = [CIContext contextWithEAGLContext:glContext];
    self.contextCPU = [CIContext contextWithOptions: nil];    
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect {
   
    [self.contextGPU drawImage:self.imagenFiltrada inRect:view.bounds fromRect:self.imagenFiltrada.extent];
}

- (void)viewDidUnload
{
    [self setImageView:nil];
    [self setGlkView:nil];
    [self setContextCPU:nil];
    [self setContextGPU:nil];
    [self setImagenOriginal:nil];
    [self setImagenFiltrada:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if(!self.imagenFiltrada) {
        self.imagenFiltrada = self.imagenOriginal;        
    }
    [self.glkView display];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)sliderCpuCambia:(UISlider *)sender {
    //NSLog(@"%f",sender.value);
    // TODO (a) Realizar el filtrado utilizando el contexto CPU
    // 1. Crear filtro CISepiaTone con intensidad sender.value, y aplicarlo a self.imagenOriginal
    CIFilter *filter=[CIFilter filterWithName:@"CISepiaTone"
                      keysAndValues:kCIInputImageKey, self.imagenOriginal,
                      @"inputIntensity",[NSNumber numberWithFloat:sender.value], nil];
    // 2. Obtener salida del filtro
    CIImage *imagFilter = filter.outputImage;
    // 3. Obtener CGImageRef a partir de la imagen de salida del filtro, utilizando self.contextCPU
    CGImageRef cgImage = [self.contextCPU createCGImage:imagFilter 
                                       fromRect:imagFilter.extent];
    
    // 4. Mostrar la imagen resultante el self.imageView (construir UIImage a partir de CGImageRef)
    
    self.imageView.image = [UIImage imageWithCGImage:cgImage];
    // TODO (c) Encadenar filtro CIHueAdjust a la salida del filtro anterior


}

- (IBAction)sliderGpuCambia:(UISlider *)sender {
    
    // TODO (b) Realizar el filtrado utilizando el contexto GPU
    // 1. Crear filtro CISepiaTone con intensidad sender.value, y aplicarlo a self.imagenOriginal
    CIFilter *filter=[CIFilter filterWithName:@"CISepiaTone"
                                keysAndValues:kCIInputImageKey, self.imagenFiltrada,
                      @"inputIntensity",[NSNumber numberWithFloat:sender.value], nil];

    // 2. Obtener salida del filtro
    CIImage *imagFilter = filter.outputImage;
    
    // 3. Guardar la salida del filtro en self.imagenFiltrada
    CGImageRef cgImage = [self.contextGPU createCGImage:imagFilter 
                                               fromRect:imagFilter.extent];
    self.imagenFiltrada=[CIImage imageWithCGImage:cgImage];
    // TODO (c) Encadenar filtro CIHueAdjust a la salida del filtro anterior

    
    [self.glkView display]; 
}

- (IBAction)agregarFoto:(id)sender {
    // TODO (d) Guardar la imagen self.imageView.image en el album de fotos del dispositivo


}

- (void)dealloc {
    [_imageView release];
    [_glkView release];
    [_contextGPU release];
    [_contextCPU release];
    [_imagenOriginal release];
    [_imagenFiltrada release];
    [super dealloc];
}
@end
