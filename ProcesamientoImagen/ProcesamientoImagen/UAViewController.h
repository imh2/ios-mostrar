//
//  UAViewController.h
//  ProcesamientoImagen
//
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreImage/CoreImage.h>
#import <GLKit/GLKit.h>

@interface UAViewController : UIViewController<GLKViewDelegate> {
    BOOL _procesando;
}

@property (retain, nonatomic) CIContext *contextGPU;
@property (retain, nonatomic) CIContext *contextCPU;

@property (retain, nonatomic) CIImage *imagenOriginal;
@property (retain, nonatomic) CIImage *imagenFiltrada;
@property (retain, nonatomic) IBOutlet UIImageView *imageView;
@property (retain, nonatomic) IBOutlet GLKView *glkView;

- (IBAction)sliderGpuCambia:(UISlider *)sender;
- (IBAction)sliderCpuCambia:(UISlider *)sender;
- (IBAction)agregarFoto:(id)sender;

@end
