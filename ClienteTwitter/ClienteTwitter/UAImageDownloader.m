//
//  ImageDownloader.m
//  Biblioteca
//

#import "UAImageDownloader.h"

@implementation UAImageDownloader

@synthesize connection = _connection;
@synthesize data = _data;
@synthesize url = _url;
@synthesize image = _image;

@synthesize indexPath = _indexPath;
@synthesize delegate = _delegate;

- (id)initWithUrl: (NSString *) url indexPath: (NSIndexPath *)indexPath delegate: (id<UAImageDownloaderDelegate>) delegate;
{
    self = [super init];
    if (self) {
        NSURL *urlImagen = [NSURL URLWithString: url];
        NSURLRequest *theRequest = [NSURLRequest requestWithURL: urlImagen];
        NSURLConnection *theConnection = [NSURLConnection connectionWithRequest: theRequest delegate: self];        
        
        self.url = url;
        self.indexPath = indexPath;
        self.delegate = delegate;
        
        self.connection = theConnection;
    }
    
    return self;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    if(response.expectedContentLength != NSURLResponseUnknownLength) {
        self.data = [NSMutableData dataWithCapacity: response.expectedContentLength];
    } else {
        self.data = [NSMutableData data];
    }
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data 
{    
    [self.data appendData: data];
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection 
{
    UIImage *theImage = [UIImage imageWithData: self.data];
    self.image = theImage;
    
    [self.delegate imageDownloader: self didFinishDownloadingImage:self.image forIndexPath:self.indexPath];
}

- (void) dealloc 
{
    [_connection release];
    [_data release];
    [_url release];
    [_image release];
    [super dealloc];
}

@end
