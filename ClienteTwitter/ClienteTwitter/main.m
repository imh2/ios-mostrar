//
//  main.m
//  ClienteTwitter
//
//  Created by Miguel Angel Lozano on 31/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UAAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([UAAppDelegate class]));
    }
}
