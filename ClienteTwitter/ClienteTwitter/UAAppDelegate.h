//
//  UAAppDelegate.h
//  ClienteTwitter
//


#import <UIKit/UIKit.h>

@interface UAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *navigationController;

@end
