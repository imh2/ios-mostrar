//
//  UADetailViewController.m
//  ClienteTwitter
//


#import "UADetallesTweetViewController.h"

#import "UATweet.h"

@interface UADetallesTweetViewController ()
- (void)configureView;
@end

@implementation UADetallesTweetViewController

@synthesize detailItem = _detailItem;
@synthesize imageViewPerfil = _imageViewPerfil;
@synthesize textLabelUsuario = _textLabelUsuario;
@synthesize textViewMensaje = _textViewMensaje;

@synthesize content = _content;
@synthesize connection = _connection;

- (void)dealloc
{
    [_content release];
    [_connection release];
    [_detailItem release];
    [_imageViewPerfil release];
    [_textLabelUsuario release];
    [_textViewMensaje release];
    [super dealloc];
}

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        [_detailItem release]; 
        _detailItem = [newDetailItem retain]; 

        // Update the view.
        [self configureView];
    }
}

- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.detailItem) {
        self.textLabelUsuario.text = [self.detailItem usuario];
        self.textViewMensaje.text = [self.detailItem texto];
        
        if([self.detailItem imagen]) {
            self.imageViewPerfil.image = [self.detailItem imagen];
        } else {
            
            // TODO: Crea conexión para obtener [self.detailItem urlImagen] (sesión 1)
            // 1. Crear la URL con la cadena [self.detailItem urlImagen]
            // 2. Crea la petición a partir de la URL
            // 3. Si self.connection está creada (distinta de nil), cancelala (cancel)
            // 4. Inicializa la conexión y guardala en self.connection, usando self como delegada
            // 5. Muestra indicador de actividad de red

        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)viewDidUnload
{
    [self setImageViewPerfil:nil];
    [self setTextLabelUsuario:nil];
    [self setTextViewMensaje:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Datos del tweet";
    }
    return self;
}		

#pragma mark - NSURLConnection delegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    if(response.expectedContentLength != NSURLResponseUnknownLength) {
        self.content = [NSMutableData dataWithCapacity: response.expectedContentLength];
    } else {
        self.content = [NSMutableData data];
    }
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data 
{    
    [self.content appendData: data];        
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    self.connection = nil;
    NSLog(@"Error al recuperar la imagen");
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection 
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;    
    
    // TODO: Crea una imagen a partir de los datos recibidos (self.content) (sesión 1)
    UIImage *theImage = nil; // <-- Crear imagen a partir de self.content (NSData)
    
    [self.detailItem setImagen: theImage];
    self.imageViewPerfil.image = [self.detailItem imagen];
}

@end
