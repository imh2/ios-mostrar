//
//  UAMasterViewController.m
//  ClienteTwitter


#import <Twitter/Twitter.h>
#import <Accounts/Accounts.h>
#import "UAListadoTweetsViewController.h"

#import "UADetallesTweetViewController.h"
#import "UATweet.h"
#import "UAImageDownloader.h"


@interface UAListadoTweetsViewController()

- (void) cargarImagen: (UATweet *) tweet paraIndexPath: (NSIndexPath *) indexPath;
- (void) actualizarTweetsConDatos: (NSData *) data;
- (void) inicializaTweets;

@end


@implementation UAListadoTweetsViewController

@synthesize detailViewController = _detailViewController;
@synthesize content = _content;
@synthesize connection = _connection;
@synthesize downloadingImages = _downloadingImages;
@synthesize listadoTweets = _listadoTweets;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Tweets";
        self.listadoTweets = nil;
        
        UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithTitle:@"Tweet" style:UIBarButtonItemStylePlain target:self action:@selector(nuevoTweet:)];
        self.navigationItem.rightBarButtonItem = barButton;
        [barButton release];
    }
    return self;
}
	
- (void) inicializaTweets {
    self.listadoTweets = [NSMutableArray arrayWithCapacity:20];
    self.downloadingImages = [NSMutableDictionary dictionaryWithCapacity:20];
    UATweet *tweet;
    
    tweet = [[UATweet alloc] init];
    tweet.texto = @"Arrived MAG :)";
    tweet.usuario = @"njel_njel";
    tweet.urlImagen = @"http://a3.twimg.com/profile_images/1700132019/angel_20dn_20ko_20lee_normal.jpg";
    [self.listadoTweets addObject: tweet];
    [tweet release];

    tweet = [[UATweet alloc] init];
    tweet.texto = @"Bicara tentang cinta?Oh haruskha";
    tweet.usuario = @"deck_duarrma";
    tweet.urlImagen = @"http://a0.twimg.com/sticky/default_profile_images/default_profile_5_normal.png";
    [self.listadoTweets addObject: tweet];
    [tweet release];

    tweet = [[UATweet alloc] init];
    tweet.texto = @"Eu mando sms pro @Prazer_Asafe e não sei se vai DDD:";
    tweet.usuario = @"73_2nd";
    tweet.urlImagen = @"http://a2.twimg.com/profile_images/1729093641/image_normal.jpg";
    [self.listadoTweets addObject: tweet];
    [tweet release];

    tweet = [[UATweet alloc] init];
    tweet.texto = @"Yuk pindah chanel TVRI JATENG nonton orkes keroncong";
    tweet.usuario = @"kazchangmin";
    tweet.urlImagen = @"http://a3.twimg.com/profile_images/1430424877/house_normal.jpg";
    [self.listadoTweets addObject: tweet];
    [tweet release];

    tweet = [[UATweet alloc] init];
    tweet.texto = @"Ballard File Cabinet with Silver Printed Tempered Glass Top - Eur Style: Part of collection by Eur Style, this File http://t.co/e7Pk0F4V";
    tweet.usuario = @"2hikarisogo";
    tweet.urlImagen = @"http://a0.twimg.com/profile_images/1694597079/bf65254214_normal.jpg";
    [self.listadoTweets addObject: tweet];
    [tweet release];

    tweet = [[UATweet alloc] init];
    tweet.texto = @"Penk tambah cakep kalo rambutnya dipotong";
    tweet.usuario = @"ma_china";
    tweet.urlImagen = @"http://a1.twimg.com/profile_images/1671830495/2444172_3182143287_normal.jpg";
    [self.listadoTweets addObject: tweet];
    [tweet release];

    tweet = [[UATweet alloc] init];
    tweet.texto = @"NF @sharonaaa_ x";
    tweet.usuario = @"Superhero_ine";
    tweet.urlImagen = @"http://a3.twimg.com/profile_images/1763521726/333199125_normal.jpg";
    [self.listadoTweets addObject: tweet];
    [tweet release];

    tweet = [[UATweet alloc] init];
    tweet.texto = @"Mtk sama bk doang kaayanye RT @monikk_iicha: apaaan aje sih ma?";
    tweet.usuario = @"Rahmadsftr";
    tweet.urlImagen = @"http://a3.twimg.com/profile_images/1774993236/cats29__E5_89_AF_E6_9C_AC_normal.jpg";
    [self.listadoTweets addObject: tweet];
    [tweet release];

    tweet = [[UATweet alloc] init];
    tweet.texto = @"Solo pido ver en tu mirada,el mismo sentimiento,felicidad...el mismo deseo...el brillo de ojos que yo siento cuando te veo.";
    tweet.usuario = @"fmtmittan";
    tweet.urlImagen = @"http://a3.twimg.com/profile_images/1653731016/siirareteiru_normal.png";
    [self.listadoTweets addObject: tweet];
    [tweet release];

    tweet = [[UATweet alloc] init];
    tweet.texto = @"I'm at Marina Büfe http://t.co/20St7jkL";
    tweet.usuario = @"eknataizi";
    tweet.urlImagen = @"http://a2.twimg.com/profile_images/1655102226/image_normal.jpg";
    [self.listadoTweets addObject: tweet];
    [tweet release];

    tweet = [[UATweet alloc] init];
    tweet.texto = @"Sony Xperia Kumquat leaked picture hits the web ~ Latest Apple iPad Games Reviews http://t.co/P41LrRhT";
    tweet.usuario = @"drdluck";
    tweet.urlImagen = @"http://a2.twimg.com/profile_images/1632785283/image_normal.jpg";
    [self.listadoTweets addObject: tweet];
    [tweet release];

    tweet = [[UATweet alloc] init];
    tweet.texto = @"My week on twitter: 1 retweets received, 18 new followers, 2 mentions. Via: http://t.co/lYpQReXJ";
    tweet.usuario = @"timeless_table";
    tweet.urlImagen = @"http://a3.twimg.com/profile_images/1715374531/timeless_table_logo_brown_normal.jpg";
    [self.listadoTweets addObject: tweet];
    [tweet release];

    tweet = [[UATweet alloc] init];
    tweet.texto = @"Kmren gk gtu prjanjiannya sepatu yg km kirim gk sbanding sma PH gila";
    tweet.usuario = @"nisaaaaAR";
    tweet.urlImagen = @"http://a3.twimg.com/profile_images/1738703335/ca_normal.jpg";
    [self.listadoTweets addObject: tweet];
    [tweet release];

    tweet = [[UATweet alloc] init];
    tweet.texto = @"Isso, não por opção, já faz parte de mim há muito tempo. Há muito mais tempo do que eu possa me... http://t.co/kb68BCeT";
    tweet.usuario = @"MinnieClukks";
    tweet.urlImagen = @"http://a2.twimg.com/profile_images/1597002687/image_normal.jpg";
    [self.listadoTweets addObject: tweet];
    [tweet release];

    tweet = [[UATweet alloc] init];
    tweet.texto = @"Inameh pronostica precipitaciones dispersas en varios estado del país - http://t.co/VdKBozQN";
    tweet.usuario = @"MARACAYINFORMA";
    tweet.urlImagen = @"http://a2.twimg.com/profile_images/1739827881/anigifmcyPRUEBA_normal.gif";
    [self.listadoTweets addObject: tweet];
    [tweet release];

    tweet = [[UATweet alloc] init];
    tweet.texto = @"RT @cloudtimesorg: France's Cloud computing market set to exceed EUR 2 billion in 2011 http://t.co/JqIRQga5";
    tweet.usuario = @"faridelbaraka";
    tweet.urlImagen = @"http://a0.twimg.com/profile_images/1567938535/X34sq2fi_normal";
    [self.listadoTweets addObject: tweet];
    [tweet release];

    tweet = [[UATweet alloc] init];
    tweet.texto = @"Can't get out of bed";
    tweet.usuario = @"yukimasa_bot";
    tweet.urlImagen = @"http://a0.twimg.com/profile_images/1141472015/yukimasabot_normal.png";
    [self.listadoTweets addObject: tweet];
    [tweet release];

    tweet = [[UATweet alloc] init];
    tweet.texto = @"This --&gt; RT @IamLaraTorrella: I called your boyfriend and he hit me with his man purse . :D";
    tweet.usuario = @"UrRightt";
    tweet.urlImagen = @"http://a0.twimg.com/profile_images/1755577202/63494847640963125_normal.jpg";
    [self.listadoTweets addObject: tweet];
    [tweet release];

    tweet = [[UATweet alloc] init];
    tweet.texto = @"Xbox 360 Windows Media Center Extender Demo - http://t.co/TuJ76l4f";
    tweet.usuario = @"xbox360video";
    tweet.urlImagen = @"http://a2.twimg.com/profile_images/441807609/xbox360video_logo_128-128_normal.jpg";
    [self.listadoTweets addObject: tweet];
    [tweet release];

    tweet = [[UATweet alloc] init];
    tweet.texto = @"Not staying here. For real.";
    tweet.usuario = @"NaydeensFiesty";
    tweet.urlImagen = @"http://a3.twimg.com/profile_images/1752792902/TPhoto_00070_normal.jpg";
    [self.listadoTweets addObject: tweet];
    [tweet release];
}

- (void)dealloc
{
    [_content release];
    [_connection release];
    [_detailViewController release];
    [_downloadingImages release];
    [_listadoTweets release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

    
    // TODO: Sustituir listado predefinido por acceso a servicio JSON (sesión 2)
    
    // 1. Eliminar llamada a inicializaTweets
    //[self inicializaTweets];    
    
    // 2. Conectar a URL http://search.twitter.com/search.json?q=universidad%20alicante
    
    // 3. Utilizar esta clase como delegada de la conexión asíncrona
    // 4. Mostrar indicador de actividad de red
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.listadoTweets==nil) {
        return 1;
    } else {
        return [self.listadoTweets count];        
    }
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    static NSString *LoadingCellIdentifier = @"CellLoading";
    
    UITableViewCell *cell = nil;
    
    if(self.listadoTweets==nil) {
        // Cargando el listado de tweets
        
        cell = [tableView dequeueReusableCellWithIdentifier:LoadingCellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:LoadingCellIdentifier] autorelease];

            cell.textLabel.text = @"Cargando";
            
            UIActivityIndicatorView *theProgress = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleGray];
            [theProgress startAnimating];
            cell.accessoryView = theProgress;
            [theProgress release];
        }

    } else {
        // Listado de tweets descargado
        
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }

        // Configure the cell.
        UATweet *theTweet = [self.listadoTweets objectAtIndex: indexPath.row];
        cell.textLabel.text = theTweet.texto;
        cell.detailTextLabel.text = theTweet.usuario;
        
        if(theTweet.imagen!=nil) {
            cell.imageView.image = theTweet.imagen;
        } else if(theTweet.urlImagen!=nil) {
            [self cargarImagen: theTweet paraIndexPath: indexPath];
            cell.imageView.image = [UIImage imageNamed: @"Placeholder.png"];
        }
    }

    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.detailViewController) {
        self.detailViewController = [[[UADetallesTweetViewController alloc] initWithNibName:@"UADetallesTweetView" bundle:nil] autorelease];
    }
    self.detailViewController.detailItem = [self.listadoTweets objectAtIndex: indexPath.row];
    
    [self.navigationController pushViewController:self.detailViewController animated:YES];
}

#pragma mark - NSURLConnection delegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    if(response.expectedContentLength != NSURLResponseUnknownLength) {
        self.content = [NSMutableData dataWithCapacity: response.expectedContentLength];
    } else {
        self.content = [NSMutableData data];
    }
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data 
{    
    [self.content appendData: data];        
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    self.connection = nil;
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection 
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;    
    [self actualizarTweetsConDatos: self.content];
}

- (void) actualizarTweetsConDatos: (NSData *) data {
    NSError *error = nil;
    
    // TODO: Obtener NSArray con la lista de tweets a partir de los datos JSON (atributo "results" del objeto NSDictionary obtenido) (sesión 2)
    NSArray *tweets = nil; // <-- Leer JSON
    
    if(error==nil) {
        self.listadoTweets = [NSMutableArray arrayWithCapacity:20];
        self.downloadingImages = [NSMutableDictionary dictionaryWithCapacity:20];
        
        for(NSDictionary *tweet in tweets) {
            UATweet *theTweet = [[UATweet alloc] init];
            
            theTweet.texto = nil; // <-- Obtener atributo "text"
            theTweet.usuario = nil; // <-- Obtener atributo "from_user"
            theTweet.urlImagen = nil; // <-- Obtener atributo "profile_image_url"
            
            [self.listadoTweets addObject: theTweet];
            [theTweet release];
        }
        
    } else {
        NSLog(@"Error al procesar JSON");         
    }
    
    [self.tableView reloadData];
}


#pragma mark - Carga de imagenes

- (void) imageDownloader:(UAImageDownloader *)downloader didFinishDownloadingImage:(UIImage *)image forIndexPath:(NSIndexPath *)indexPath {
    
    // TODO: Mostrar la imagen cargada en la tabla (sesión 1)
    
    // 1. Obtener el objeto UATweet en la posición indexPath.row de la lista self.listadoTweets
    // 2. Asignar la imagen a la propiedad imagen del tweet
    // 3. Obtener la celda (UITableViewCell) de la posición indexPath de self.tableView 
    // 4. Mostrar la imagen en dicha celda, asignándola a la propiedad cell.imageView.image
}

- (void) cargarImagen: (UATweet *) tweet paraIndexPath: (NSIndexPath *) indexPath
{
    if([self.downloadingImages objectForKey: indexPath] == nil) {
        
        // TODO: Crea un UAImageDownloader para descargar tweet.urlImagen en indexPath, utilizando self como delegada, y añadirlo al diccionario self.downloadingImages indexado con la clave indexPath (sesión 1)
    }
}

#pragma mark - Eventos para envio de tweets

- (void) nuevoTweet: (id) sender {
    // TODO: Enviar tweet con interfaz predefinida en iOS 5 (sesión 3)
    TWTweetComposeViewController *controller = 
    [[TWTweetComposeViewController alloc] init];
    [controller setInitialText: @"Este tweet ha sido enviado desde la Aplicacion: ClienteTwitterUA hecho en iOS"];
    [self presentModalViewController:controller animated: YES];
    
}

@end
