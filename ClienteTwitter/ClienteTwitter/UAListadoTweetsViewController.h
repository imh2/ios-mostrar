//
//  UAMasterViewController.h
//  ClienteTwitter
//


#import <UIKit/UIKit.h>

#import "UAImageDownloader.h"

@class UADetallesTweetViewController;

@interface UAListadoTweetsViewController : UITableViewController<UAImageDownloaderDelegate, NSURLConnectionDataDelegate>

@property (strong, nonatomic) UADetallesTweetViewController *detailViewController;

@property(nonatomic,strong) NSURLConnection *connection;
@property(nonatomic,strong) NSMutableData *content;

@property(nonatomic,strong) NSMutableDictionary *downloadingImages;

@property(nonatomic,strong) NSMutableArray *listadoTweets;

@end
