//
//  UATweet.m
//  ClienteTwitter
//
//  Created by Miguel Angel Lozano on 31/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "UATweet.h"

@implementation UATweet

@synthesize texto = _texto;
@synthesize usuario = _usuario;
@synthesize urlImagen = _urlImagen;
@synthesize imagen = _imagen;


- (void)dealloc {
    [_texto release];
    [_usuario release];
    [_urlImagen release];
    [_imagen release];
    [super dealloc];
}

@end
