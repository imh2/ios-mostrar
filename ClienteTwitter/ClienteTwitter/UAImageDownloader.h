//
//  ImageDownloader.h
//  Biblioteca


#import <Foundation/Foundation.h>

@class UAImageDownloader;

@protocol UAImageDownloaderDelegate

- (void) imageDownloader: (UAImageDownloader *) downloader didFinishDownloadingImage: (UIImage *) image forIndexPath: (NSIndexPath *) indexPath;

@end

@interface UAImageDownloader : NSObject<NSURLConnectionDataDelegate>

- (id)initWithUrl: (NSString *) url indexPath: (NSIndexPath *)indexPath delegate: (id<UAImageDownloaderDelegate>) delegate;

@property(nonatomic,strong) NSString *url;
@property(nonatomic,strong) NSURLConnection *connection;
@property(nonatomic,strong) NSMutableData *data;
@property(nonatomic,strong) UIImage *image;

@property(nonatomic,unsafe_unretained) id<UAImageDownloaderDelegate> delegate;
@property(nonatomic,unsafe_unretained) NSIndexPath *indexPath;

@end
