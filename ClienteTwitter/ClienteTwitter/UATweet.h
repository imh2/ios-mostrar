//
//  UATweet.h
//  ClienteTwitter
//
//  Created by Miguel Angel Lozano on 31/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UATweet : NSObject

@property(nonatomic,strong) NSString *texto;
@property(nonatomic,strong) NSString *usuario;
@property(nonatomic,strong) NSString *urlImagen;
@property(nonatomic,strong) UIImage *imagen;

@end
