//
//  UADetailViewController.h
//  ClienteTwitter
//


#import <UIKit/UIKit.h>

@interface UADetallesTweetViewController : UIViewController<NSURLConnectionDataDelegate>

@property(nonatomic,strong) NSURLConnection *connection;
@property(nonatomic,strong) NSMutableData *content;

@property (strong, nonatomic) id detailItem;

@property (retain, nonatomic) IBOutlet UIImageView *imageViewPerfil;
@property (retain, nonatomic) IBOutlet UILabel *textLabelUsuario;
@property (retain, nonatomic) IBOutlet UITextView *textViewMensaje;


@end
