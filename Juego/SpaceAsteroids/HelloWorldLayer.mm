//
//  HelloWorldLayer.m
//  SpaceAsteroids
//
//   Copyright __MyCompanyName__ 2013. All rights reserved.
//


// Import the interfaces
#import "HelloWorldLayer.h"

// Needed to obtain the Navigation Controller
#import "AppDelegate.h"
#import "UAGameLayer.h"
#import "UAAboutLayer.h"

#pragma mark - HelloWorldLayer

// HelloWorldLayer implementation
@implementation HelloWorldLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorldLayer *layer = [HelloWorldLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {
		
        // TODO 1.b Cambia el titulo a "Space Asteroids"
        // TODO 1.c Cambia la etiqueta a tipo bitmap font, con nuestro propio tipo de fuente
        
		// create and initialize a Label
		//CCLabelTTF *label = [CCLabelTTF labelWithString:@"Space Asteroids" fontName:@"Marker Felt" fontSize:64];
        
        CCLabelBMFont *label = [CCLabelBMFont labelWithString:@"Space Asteroids" fntFile:@"Arial.fnt"];

		// ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
	
		// position the label on the center of the screen
		label.position =  ccp( size.width /2 , size.height/2 );
		
		// add the label as a child to this Layer
		[self addChild: label];

        
		// Default font size will be 28 points.
		[CCMenuItemFont setFontSize:28];
		
		// TODO 1.d Cambia la opción de menu a tipo item imagen, con boton_start
        
		//CCMenuItem *itemAchievement = [CCMenuItemFont itemWithString:@"Achievements" block:^(id sender) {}];
        
        CCMenuItemImage *itemStart = [CCMenuItemImage 
                                   itemFromNormalImage:@"boton_start_normal.png"
                                   selectedImage:@"boton_start_press.png"
                                   block:^(id sender) {

            // TODO 1.h Haz que se haga una transición a UAGameLayer
            [[CCDirector sharedDirector] replaceScene: 
            [CCTransitionPageTurn transitionWithDuration:0.5f 
                                scene:[UAGameLayer scene]]];
		

		// TODO 1.d Cambia la opción de menu a tipo item imagen, con boton_about
        }];
        
        CCMenuItemImage *itemAbout = [CCMenuItemImage itemFromNormalImage:@"boton_about_normal.png"
                                                            selectedImage:@"boton_about_press.png" block:^(id sender) {
			
            // TODO 1.f Haz que al pulsar about pase a la pantalla UAAboutLayer
            //[[CCDirector sharedDirector] replaceScene: [UAAboutLayer scene]];
                                                                
            // TODO 1.g Haz que el cambio de escena se haga mediante una transicion
            [[CCDirector sharedDirector] replaceScene: 
            [CCTransitionJumpZoom transitionWithDuration:0.5f 
                scene:[UAAboutLayer scene]]];

		}];
		
		CCMenu *menu = [CCMenu menuWithItems:itemStart, itemAbout, nil];
		
		[menu alignItemsHorizontallyWithPadding:10];
		[menu setPosition:ccp( size.width/2, size.height/2 - 60)];
		
		// Add the menu to the layer
		[self addChild:menu];

	}
	return self;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}

@end
