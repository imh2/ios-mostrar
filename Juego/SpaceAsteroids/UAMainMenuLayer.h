//
//  UAMainMenuLayer.h
//  SpaceAsteroids
//
//   Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface UAMainMenuLayer : CCLayer {
    
}

- (void)playGame:(id)sender;
- (void)about:(id)sender;

@end
