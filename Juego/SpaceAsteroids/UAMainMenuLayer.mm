//
//  UAMainMenuLayer.mm
//  SpaceAsteroids
//
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "UAMainMenuLayer.h"
#import "UAGameLayer.h"
#import "UAAboutLayer.h"
#import "SimpleAudioEngine.h"


@implementation UAMainMenuLayer

-(id) init{
    if((self=[super init])) {
    SimpleAudioEngine* audio = [SimpleAudioEngine sharedEngine];
    
    [audio playBackgroundMusic: @"menumusic.mp3" loop: YES];
    }
    return self;
}


- (void)playGame:(id)sender{
    [[CCDirector sharedDirector] replaceScene: 
     [CCTransitionPageTurn transitionWithDuration:0.5f 
                                            scene:[UAGameLayer scene]]];
    
}

- (void)about:(id)sender{
    
    [[CCDirector sharedDirector] replaceScene: 
     [CCTransitionJumpZoom transitionWithDuration:0.5f 
                                            scene:[UAAboutLayer scene]]];
    
}


@end
