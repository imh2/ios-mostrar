//
//  UAGameLayer.h
//  SpaceAsteroids
//
//  
//

#import "cocos2d.h"
#import "Box2D.h"

#define PTM_RATIO 32

@interface UAGameLayer : CCLayer<CCTargetedTouchDelegate> {
    NSInteger _numAsteroids;
    NSInteger _velocidadPersonaje;
    
    b2World *_world;
    b2Body *_bodyRoca;
    b2Body *_bodyRayo;
    
    NSUInteger _score;
    
    CGSize _tileSize;
    CGPoint _respawnPosition;
}

@property (nonatomic, retain) CCLabelBMFont *scoreLabel;

@property (nonatomic, retain) CCSprite *spritePersonaje;
@property (nonatomic, retain) CCSprite *spriteRayo;

@property (nonatomic, retain) CCArray *asteroids;

@property (nonatomic, retain) CCTMXTiledMap *tiledMap;

+(CCScene *) scene;

@end
