//
//  UAAboutLayer.mm
//  SpaceAsteroids
//
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "UAAboutLayer.h"
#import "CCBReader.h"


@implementation UAAboutLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	UAAboutLayer *layer = [UAAboutLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(id) init
{
    if( (self=[super init])) {
        // Inicializar componentes de la capa
        CCLabelTTF *labelAbout = [CCLabelTTF labelWithString:@"ToFaHiTo Products" fontName:@"Marker Felt" fontSize:32];
        
        // ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
        
		// position the label on the center of the screen
		labelAbout.position =  ccp( size.width /2 , size.height/2 );
		
		// add the label as a child to this Layer
		[self addChild: labelAbout];
        
        
		// Default font size will be 28 points.
		[CCMenuItemFont setFontSize:28];
        
    
		CCMenuItem *itemBack = [CCMenuItemFont itemWithString:@"Backentosh" block:^(id sender) {
        
           /* [[CCDirector sharedDirector] replaceScene: 
             [CCTransitionFlipAngular transitionWithDuration:0.5f 
                scene:[UAMainMenuLayer scene]]];
            */
            
            [[CCDirector sharedDirector] replaceScene:
             [CCTransitionFlipAngular transitionWithDuration:0.5f 
                                                       scene:
             [CCBReader sceneWithNodeGraphFromFile:@"MainMenu.ccbi"]]];

        
        }];
        
        CCMenu *menu = [CCMenu menuWithItems:itemBack,nil];
		
		[menu alignItemsHorizontallyWithPadding:20];
		[menu setPosition:ccp( size.width/2, size.height/2 - 50)];
		
		// Add the menu to the layer
		[self addChild:menu];


    }
    return self;
}

@end
