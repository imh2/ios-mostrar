//
//  UAGameLayer.m
//  SpaceAsteroids
//
//

#import "UAGameLayer.h"

#import "SimpleAudioEngine.h"
#import "GB2ShapeCache.h"

@implementation UAGameLayer

@synthesize scoreLabel=_scoreLabel;
@synthesize spritePersonaje=_spritePersonaje;
@synthesize spriteRayo=_spriteRayo;
@synthesize asteroids=_asteroids;
@synthesize tiledMap=_tiledMap;

SimpleAudioEngine* audio = [SimpleAudioEngine sharedEngine];

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	UAGameLayer *layer = [UAGameLayer node];
	
    // Crea HUD
    
    CGSize screenSize = [[CCDirector sharedDirector] winSize];
    
    // TODO 3.m Crea una etiquete de tipo bitmap font con el texto Score: 00000 y nuestro propio tipo de fuente
    
    
    CCLabelBMFont *scoreLabel = [CCLabelBMFont labelWithString:@"Score 00000" fntFile:@"Arial.fnt"];
    
    
    // TODO 3.m Ajusta el anchorPoint y position para que la etiqueta aparezca en la esquina superior derecha
    scoreLabel.anchorPoint = CGPointMake(1, 1);
    scoreLabel.position = CGPointMake(screenSize.width-1,screenSize.height-1);
    
    // TODO 3.m Asigna la etiqueta a la propiedad layer.scoreLabel
    layer.scoreLabel = scoreLabel;
    
    // TODO 3.m Añade la etiqueta a la escena
    [scene addChild:scoreLabel z:1];
    

    // add layer as a child to scene
	[scene addChild: layer z:0];

	// return the scene
	return scene;
}

#pragma mark - Gestion del tilemap

- (void)centerViewport {
    CGSize screenSize = [[CCDirector sharedDirector] winSize];
    
    CGFloat offset =  screenSize.width / 2.0 - self.spritePersonaje.position.x;
    if(offset > 0) {
        offset = 0;
    } else if(offset < screenSize.width - _tileSize.width * self.tiledMap.mapSize.width) {
        offset = screenSize.width - _tileSize.width * self.tiledMap.mapSize.width;
    }
    
    self.position = ccp(offset, 0);
}

- (BOOL)isCollidableTileAt:(CGPoint)tileCoords {
    
    // Consideramos que celdas fuera del mapa no son nunca colisionables
    if(tileCoords.x < 0 || tileCoords.x >= self.tiledMap.mapSize.width || tileCoords.y < 0 || tileCoords.y >= self.tiledMap.mapSize.height) {
        return NO;
    }
    
    CCTMXLayer *layerMuros = [self.tiledMap layerNamed:@"muros"];
    
    int tileGid = [layerMuros tileGIDAt:tileCoords];
    if (tileGid) {
        NSDictionary *properties = [self.tiledMap propertiesForGID:tileGid];
        if (properties) {
            NSString *collision = [properties valueForKey:@"colisionable"];
            return (collision && [collision compare:@"true"] == NSOrderedSame);
        }
    }
    
    return NO;
}

- (CGRect)rectForTileAt:(CGPoint)tileCoords {
    float totalHeight = self.tiledMap.mapSize.height * _tileSize.height;
    CGPoint origin = ccp(tileCoords.x * _tileSize.width, totalHeight - ((tileCoords.y + 1) * _tileSize.height));
    return CGRectMake(origin.x, origin.y, _tileSize.width, _tileSize.height);
}

- (CGPoint)tileCoordForPosition:(CGPoint)position
{
    float totalHeight = self.tiledMap.mapSize.height * _tileSize.height;
    float x = floor(position.x / _tileSize.width);
    float y = floor((totalHeight - position.y) / _tileSize.height);
    return ccp(x, y);
}


- (NSInteger)generateRandomNumberFrom:(NSInteger)min to:(NSInteger)max {
    return rand() % (max - min) + min;
}



// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) {
        
        self.isTouchEnabled = YES;
        [[[CCDirector sharedDirector] touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
        _respawnPosition = ccp(240, 37);
		_velocidadPersonaje = 0;
        _numAsteroids = 5;
        _score = 0;

        // TODO 2.b Cargar sprite sheet sheet.plist en la cache de frames de sprites
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: @"sheet.plist"];
        

        // TODO 2.c Planificar la llamada a update: en cada tick del ciclo del juego
        [self schedule: @selector(update:)];
        
        
        // Definición de las animaciones
        
        // TODO 2.f Crear animación "animacionIzq" a partir de los fotogramas pers02.png y pers03.png, con retardo 0.25, y añadirla a la cache de animaciones
        CCAnimation *animacionIzq = [CCAnimation animation];
        
        [animacionIzq addSpriteFrame: [[CCSpriteFrameCache sharedSpriteFrameCache]
                                    spriteFrameByName: @"pers02.png"]];
        
        [animacionIzq addSpriteFrame: [[CCSpriteFrameCache sharedSpriteFrameCache]
                                    spriteFrameByName: @"pers03.png"]];
        
        animacionIzq.delayPerUnit = 0.25;
        
        [[CCAnimationCache sharedAnimationCache] addAnimation: animacionIzq 
           
                                                         name: @"animacionIzq"];

        // TODO 2.f Crear animación "animacionDer" a partir de los fotogramas pers04.png y pers05.png, con retardo 0.25, y añadirla a la cache de animaciones

        CCAnimation *animacionDer = [CCAnimation animation];
        
        [animacionDer addSpriteFrame: [[CCSpriteFrameCache sharedSpriteFrameCache]
                                       spriteFrameByName: @"pers04.png"]];
        
        [animacionDer addSpriteFrame: [[CCSpriteFrameCache sharedSpriteFrameCache]
                                       spriteFrameByName: @"pers05.png"]];
        
        animacionDer.delayPerUnit = 0.25;
        
        [[CCAnimationCache sharedAnimationCache] addAnimation: animacionDer 
                                                         name: @"animacionDer"];
        
        
        
        
        // TODO 2.j Crear animación "animacionExpl" a partir de los fotogramas expl01.png, expl02.png, explo03.png, y expl04.png, con retardo 0.05, y añadirla a la cache de animaciones
        CCAnimation *animacionExpl = [CCAnimation animation];
        
        [animacionExpl addSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache]spriteFrameByName:@"expl01.png"]];
        
        [animacionExpl addSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache]spriteFrameByName:@"expl02.png"]];
        
        [animacionExpl addSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache]spriteFrameByName:@"expl03.png"]];
        
        [animacionExpl addSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache]spriteFrameByName:@"expl04.png"]];
        
        animacionExpl.delayPerUnit = 0.05;
        
        [[CCAnimationCache sharedAnimationCache] addAnimation: animacionExpl 
                                                         name: @"animacionExpl"];

        
        // Sprite batch
        
        // TODO 2.h Crear sprite batch a partir de textura sheet.png
        CCSpriteBatchNode *spriteBatch = [CCSpriteBatchNode batchNodeWithFile:@"sheet.png"];
        
        // TODO 2.h Añadir sprite batch a la capa
        [self addChild:spriteBatch];

        
        
        // Sprite personaje
                
        // TODO 2.b Crear sprite del personajes en self.spritePersonaje con frame pers01.png y posicionarlo en _respawnPosition
        self.spritePersonaje = [CCSprite spriteWithSpriteFrameName:@"pers01.png"];
        
        self.spritePersonaje.position = _respawnPosition;
        
        
        // TODO 2.b Añadir el sprite a la capa
        //[self addChild:self.spritePersonaje];
        
        
        // TODO 2.h En lugar de añadirlo a la capa, añadirlo al sprite batch
        [spriteBatch addChild: self.spritePersonaje];
        
        
        // Sprite rayo
        
        // TODO 2.d Añadir sprite self.spriteRayo con frame rayo.png
        self.spriteRayo = [CCSprite spriteWithSpriteFrameName:@"rayo.png"];
        
        // TODO 2.d Hacer el sprite invisible
        self.spriteRayo.visible= NO;
        
        // TODO 2.d Añadir el sprite a la capa
        //[self addChild:self.spriteRayo];
        
        
        // TODO 2.h En lugar de añadirlo a la capa, añadirlo al sprite batch
        [spriteBatch addChild: self.spriteRayo];
        
        
        
        // Sprites asteroides
        
        self.asteroids = [CCArray arrayWithCapacity:_numAsteroids];
        
        /*

        // TODO 2.a Crear un sprite con la imagen roca.png
        //CCSprite *asteroidRoca = [CCSprite spriteWithFile: @"roca.png"];
        
        
        // TODO 2.h Crear el sprite a partir del frame roca.png, en lugar de la imagen
        CCSprite *asteroidRoca = [CCSprite spriteWithSpriteFrameName: @"roca.png"];
        
        // TODO 2.a Añadir el sprite a self.asteroids
        [self.asteroids addObject:asteroidRoca];
        
        
        // TODO 2.a Posicionar el sprite en 240, 250
        asteroidRoca.position = ccp(240, 250);
        
        
        // TODO 2.a Añadir el sprite a la capa
        //[self addChild:asteroidRoca];
        
        
        
        // TODO 2.h En lugar de añadirlo a la capa, añadirlo al sprite batch
        [spriteBatch addChild:asteroidRoca];
        
        */
        
        
        // TODO 2.i Sustituir el código anterior por un for para crear crear _numAsteroids asteroides. Para cada asteroide:
        for (int i=0; i<_numAsteroids ; i++) {
            
            // TODO 2.i    - Se construirá a partir del frame roca.png
            CCSprite *asteroidRoca = [CCSprite spriteWithSpriteFrameName: @"roca.png"];
            
            // TODO 2.i    - Haremos que inicialmente sea invisible
            asteroidRoca.visible = NO;
            
            // TODO 2.i    - Lo añadiremos a self.asteroids
            [self.asteroids addObject:asteroidRoca];
            
            // TODO 2.i    - Lo añadiremos al sprite batch
             [spriteBatch addChild:asteroidRoca];
        }
        
        
    
        // Escenario
        
        // TODO 3.b Cargar mapa tmx en self.tiledMap
        self.tiledMap = [CCTMXTiledMap tiledMapWithTMXFile: @"tiledmap.tmx"];
        
        // TODO 3.b Añade el mapa a la capa
        //[self addChild:self.tiledMap z:1];

        // TODO 3.c Guardar en _tileSize el tamaño de los tiles del mapa en puntos
        _tileSize.height = self.tiledMap.tileSize.height;
        _tileSize.width = self.tiledMap.tileSize.width;
        
        // TODO 3.c Almacena en _respawnPosition la posición (240, _tileSize.height + self.spritePersonaje.contentSize.height / 2) para que el personaje ande sobre el suelo
        _respawnPosition = ccp(240, _tileSize.height + self.spritePersonaje.contentSize.height / 2);
        
        // TODO 3.c Posiciona el personaje en _respawnPosition
        self.spritePersonaje.position = _respawnPosition;
        
        
        // TODO 3.e Carga la capa de objetos definida en el fichero TMX
        CCTMXObjectGroup *objetos = [self.tiledMap objectGroupNamed:@"objetos"];
        
        // TODO 3.e Obten el diccionario de propiedades correspondiente al objeto "inicio"
        NSDictionary *inicio = [objetos objectNamed:@"inicio"];
        
        // TODO 3.e Obten la propiedad "x" de dicho objeto
        int inicioX = [[inicio objectForKey:@"x"] intValue];
        
        NSLog(@"x inicio %d",inicioX);
        
        // TODO 3.e Calcula ahora _respawnPosition utilizando este valor como coordenada x, y situa el personaje en dicha posición inicial
        _respawnPosition = ccp(inicioX, _tileSize.height + self.spritePersonaje.contentSize.height / 2);
        
        self.spritePersonaje.position = _respawnPosition;

        // TODO 3.f Centrar la capa en el personaje con <code>centerViewport
        [self centerViewport];
        

        // Scroll paralax
        

        // TODO 3.h Cargar fondo de montañas
        CCTMXTiledMap *fondoMontanias = [CCTMXTiledMap tiledMapWithTMXFile: @"fondomontanias.tmx"];
        // TODO 3.h Cargar fondo de estrellas
        CCTMXTiledMap *fondoEstrellas = [CCTMXTiledMap tiledMapWithTMXFile: @"fondoestrellas.tmx"];

        // TODO 3.h Crear nodo de scroll parallax
        CCParallaxNode *parallax = [CCParallaxNode node];
        
        // TODO 3.h Añadir al scroll parallax el fondo original con ratio (1,1) y z 3 (ya no deberemos añadir este fondo directamente a la capa del juego)
        [parallax addChild:self.tiledMap z:3 parallaxRatio:ccp(1,1) positionOffset:CGPointZero];
        
        // TODO 3.h Añadir al scroll parallax el fondo de montañas con ratio (0.25,1) y z 2
        [parallax addChild:fondoMontanias z:2 parallaxRatio:ccp(0.25,1) positionOffset:CGPointZero];
        
        // TODO 3.h Añadir al scroll parallax el fondo de estrellas con ratio (0.01,1) y z 1
        [parallax addChild:fondoEstrellas z:1 parallaxRatio:ccp(0.01,1) positionOffset:CGPointZero];
        
        
        // TODO 3.h Añadir el nodo de scroll parallax a la capa del juego con z -1
        [self addChild:parallax z:-1];
        
        // Inicialización del audio
        
        
        // TODO 3.l Ajustar el volumen de los efectos de sonido a 0.8
        [audio setEffectsVolume: 0.8];
        
        // TODO 3.l Ajustar el volumen de la musica a 0.6
        [audio setBackgroundMusicVolume: 0.6];
        
        
        // TODO 3.i Precargar efecto disparo.caf
        [audio preloadEffect:@"disparo.caf"];
        
        // TODO 3.i Precargar efecto explosion.caf
        [audio preloadEffect:@"explosion.caf"];
        
        // TODO 3.k Reproducir de forma ciclica bgmusic.mp3
        [audio playBackgroundMusic: @"bgmusic.mp3" loop: YES];
        
                
        // Inicialización de físicas
        
        // TODO 4.a Crear b2Vec2 con gravedad de (0,-10)
        b2Vec2 gravity;
        gravity.Set(0, -10);

        // TODO 4.a Crear mundo (b2World) en variable _world con el vector de gravedad anterior
        _world = new b2World(gravity);
        
        // Definición del asteroide
        
        // TODO 4.b Crear sprite asteroide con frame roca.png
        CCSprite *asteroideRocaRoja = [CCSprite spriteWithSpriteFrameName:@"roca.png"];
        
        // TODO 4.b Tintar el asteroide de rojo (asteroide.color = ccc3(255,0,0))
        asteroideRocaRoja.color = ccc3(255,0,0);
        
        // TODO 4.b Añadir el asteroide a la capa
        [self addChild:asteroideRocaRoja];
        
        // TODO 4.b Crear definicion del cuerpo del asteroide
        b2BodyDef bodyRocaRojaDef;
        
        // TODO 4.b Establecer el tipo como cuerpo dinámico
        bodyRocaRojaDef.type = b2_dynamicBody;
        
        // TODO 4.b Dar una velocidad líneal inicial de (1,0)
        bodyRocaRojaDef.linearVelocity.Set(1, 0);
        
        // TODO 4.b Posicionarlo inicialmente en (240,320), haciendo la conversión de pixeles a metros
        bodyRocaRojaDef.position.Set(240/PTM_RATIO, 320/PTM_RATIO);
        
        // TODO 4.b Añadir como propiedad userData el sprite de la roca tintada de rojo
        bodyRocaRojaDef.userData = asteroideRocaRoja;
        
        // TODO 4.b Crear el cuerpo a partir de la definición en la variable _bodyRoca
        _bodyRoca = _world->CreateBody(&bodyRocaRojaDef);
        
        // TODO 4.b Crear forma circular
        b2CircleShape bodyCircleShape;
        
        // TODO 4.b Dar a la forma un radio de 16px (convertidos a metros)
        bodyCircleShape.m_radius=(16/PTM_RATIO);
        
        // TODO 4.b Crear una fixture en _bodyRoca con la forma anterior
       // b2FixtureDef fixtureBodyRocaRojaDef;
        
       // fixtureBodyRocaRojaDef.shape = &bodyCircleShape;
       // fixtureBodyRocaRojaDef.restitution = 1.0;
        
       // _bodyRoca->CreateFixture(&fixtureBodyRocaRojaDef);
        
        // TODO 4.b Establecer el valor de restitution de la fixture a 1.0 para que rebote
        //fixtureBodyRocaRojaDef.restitution = 1.0;
        
        
        // TODO 4.f Eliminar la definición de la fixture anterior de la roca
        
        // TODO 4.f Cargar las formas del fichero plist generado con Physics Editor
        [[GB2ShapeCache sharedShapeCache] addShapesWithFile:@"miSuperRoca.plist"];
        
        // TODO 4.f Establecer el anchorPoint del sprite de la roca según se haya definido en sp Editor
        [asteroideRocaRoja setAnchorPoint:[[GB2ShapeCache sharedShapeCache] anchorPointForShape:@"roca"]];
        
        // TODO 4.f Añadir a _bodyRoca la fixture definida en el fichero anterior
        [[GB2ShapeCache sharedShapeCache] addFixturesToBody:_bodyRoca forShapeName:@"roca"];
        
        // Definición del suelo
        
        // TODO 4.c Creamos la definicion del cuerpo estático
        b2BodyDef bodyDefSuelo;
        
        bodyDefSuelo.type = b2_staticBody;
        
        // TODO 4.c Establecemos la posición del cuerpo en (0,0)
        bodyDefSuelo.position.Set(0, 0);
        
        // TODO 4.c Creamos el cuerpo en el mundo a partir de la definicion anterior
        b2Body *bodySuelo = _world->CreateBody(&bodyDefSuelo);
        
        // TODO 4.c Creamos una forma de tipo arista (edge)
        b2EdgeShape edgeShapeSuelo;
        
        // TODO 4.c Establecemos el punto inicial y final de la arista (convirtiendolos a metros):
        // TODO 4.c   - Inicial (0,_tileSize.height)
        // TODO 4.c   - Final (_tileSize.width * self.tiledMap.mapSize.width,_tileSize.height)
        edgeShapeSuelo.Set(b2Vec2(0/ PTM_RATIO,_tileSize.height / PTM_RATIO),b2Vec2(_tileSize.width * self.tiledMap.mapSize.width/ PTM_RATIO,_tileSize.height/ PTM_RATIO));
        
        // TODO 4.c Creamos una fixture en el cuerpo a partir de la forma anterior
        b2FixtureDef fixtureSueloDef;
        
        fixtureSueloDef.shape = &edgeShapeSuelo;
        
        bodySuelo->CreateFixture(&fixtureSueloDef);
      
        _bodyRayo = NULL;
	}
	return self;
}



#pragma mark - Acciones del personaje

- (void) moverPersonajeDerecha {
    
    // TODO 2.f Crear una acción de animación con animacionDer
    CCAnimate *animateDer = [CCAnimate actionWithAnimation:
                             [[CCAnimationCache sharedAnimationCache] 
                              animationByName:@"animacionDer"]];
    
    
    // TODO 2.f Ejecutar la acción el self.spritePersonaje
    [self.spritePersonaje runAction:[CCRepeatForever actionWithAction: animateDer]];
    
    _velocidadPersonaje = 100;
}

- (void) moverPersonajeIzquierda {
    // TODO 2.f Crear una acción de animación con animacionIzq
    CCAnimate *animateIzq = [CCAnimate actionWithAnimation:
                             [[CCAnimationCache sharedAnimationCache] 
                              animationByName:@"animacionIzq"]];
    
    
    // TODO 2.f Ejecutar la acción el self.spritePersonaje
    [self.spritePersonaje runAction:[CCRepeatForever actionWithAction: animateIzq]];
    
    
    _velocidadPersonaje = -100;
}

- (void) detenerPersonaje {
    // TODO 2.f Detener todas las acciones de self.spritePersonaje
    [self.spritePersonaje stopAllActions];
    
    // TODO 2.f Mostrar el fotograma pers01.png en el personaje
    
    [self.spritePersonaje setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"pers01.png"]];
    
    _velocidadPersonaje = 0;
}

- (void) disparar {
    if(self.spriteRayo.visible == NO) {
        CGSize screenSize = [[CCDirector sharedDirector] winSize];
        
        /*
        
        // TODO 2.e Posicionar el rayo en self.spritePersonaje.position
        self.spriteRayo.position = self.spritePersonaje.position;
        
        
        // TODO 2.e Programar una acción que mueva el rayo hasta la altura screenSize.height + self.spriteRayo.contentSize.height en 0.5 segundos (la coordenada x será la misma que la inicial)
        int rayoDesparoX = self.spritePersonaje.position.x;
        int rayoDesparoY = screenSize.height + self.spriteRayo.contentSize.height;
        
        CCMoveTo *actionRayoMoveTo = [CCMoveTo actionWithDuration:0.5
                                                         position: ccp(rayoDesparoX, rayoDesparoY)];
        
        
        // TODO 2.e Programar una acción de tipo CallBlock que haga el rayo invisible
        id blockActionRayoInvisible = [CCCallBlock actionWithBlock:(^{
			self.spriteRayo.visible = NO;
        })];
        
        
        // TODO 2.e Programar una acción que encadene las dos anteriores
        CCSequence *actionSequenceRayoDisparo = [CCSequence actions: actionRayoMoveTo, blockActionRayoInvisible, nil];
        
        
        // TODO 2.e Ejecutar la acción anterior sobre el rayo
        [self.spriteRayo runAction:actionSequenceRayoDisparo];
        */
        
        
        // TODO 4.d Sustituir el código anterior por físicas
        // TODO 4.d Crear definición del cuerpo de tipo cinemático
        
        b2BodyDef bodyDefRayo;
        bodyDefRayo.type = b2_kinematicBody;
        
        // TODO 4.d Establecer la velocidad (0,10) m/s
        bodyDefRayo.linearVelocity.Set(0,10);
        
        // TODO 4.d Establecer como posición inicial la posición del personaje (convertida a metros)
        bodyDefRayo.position.Set(self.spritePersonaje.position.x/PTM_RATIO, self.spritePersonaje.position.y/PTM_RATIO);
        
        // TODO 4.d Establecer como userData el sprite self.spriteRayo
        bodyDefRayo.userData = self.spriteRayo;
        
        // TODO 4.d Crear cuerpo _bodyRayo a partir de la definición anterior
        _bodyRayo = _world->CreateBody(&bodyDefRayo);
        
        // TODO 4.d Crear forma de tipo polígono
        b2PolygonShape shapeRayo;
        
        // TODO 4.d Definir forma como caja de dimensiones (10px, 5px) convertidas a metros
        shapeRayo.SetAsBox(10 / PTM_RATIO, 5 / PTM_RATIO);
        
        // TODO 4.d Crear fixture a partir de la forma anterior
        b2FixtureDef fixtureDefRayo;
        fixtureDefRayo.shape = &shapeRayo;
        fixtureDefRayo.restitution = 1.0;
        _bodyRayo->CreateFixture(&fixtureDefRayo);
        
        
        // TODO 2.e Hacer el rayo visible
        self.spriteRayo.visible = YES;
        
        // TODO 3.j Reproducir efecto de sonido disparo.caf
        [audio playEffect:@"disparo.caf"];
        
    }
}



- (void) update: (ccTime) dt {
    CGSize screenSize = [[CCDirector sharedDirector] winSize];

    // Actualización de la física
    
    // TODO 4.a Actualizar la física del mundo _world con dt, 6, 2
    _world->Step(dt, 6, 2);
    
    // TODO 4.a Limpiar las fuerzas del mundo
    _world->ClearForces();
    
    // TODO 4.b Obtenemos sprite de la propiedad GetUserData (de _bodyRoca)
    CCSprite *spriteBodyRocaRoja = (CCSprite *)_bodyRoca->GetUserData();
    
    // TODO 4.b Obtenemos la posición de _bodyRoca
    b2Vec2 posBodyRocaRoja = _bodyRoca->GetPosition();
    
    // TODO 4.b Obtenemos la rotación de _bodyRoca
    CGFloat rotacionBodyRocaRoja = -1 * CC_RADIANS_TO_DEGREES(_bodyRoca->GetAngle());
    
    // TODO 4.b Posicionamos el sprite de la roca en la posición del cuerpo físico (convertida a pixeles)
    spriteBodyRocaRoja.position = ccp(posBodyRocaRoja.x*PTM_RATIO, posBodyRocaRoja.y*PTM_RATIO);
    
    // TODO 4.b Establecemos la rotación del sprite a la rotación del cuerpo (convertida a grados)
    spriteBodyRocaRoja.rotation = rotacionBodyRocaRoja;
    
    
    if(_bodyRayo!=NULL) {
        // TODO 4.d Obtener sprite del rayo a partir de propiedad GetUserData() de _bodyRayo
        CCSprite *miRayoXSprite = (CCSprite*) _bodyRayo->GetUserData();
        
        // TODO 4.d Obtenemos posición de _bodyRayo
        b2Vec2 rayoPosition = _bodyRayo->GetPosition();
        
        // TODO 4.d Establecemos posición del sprite del rayo a partir de la del cuerpo (convertida a pixeles)
        miRayoXSprite.position = ccp(rayoPosition.x*PTM_RATIO, rayoPosition.y*PTM_RATIO);
        
        if(miRayoXSprite.position.y > screenSize.height /* TODO 4.d Si la posición del rayo supera el borde superior de la pantalla */) {
            
            // TODO 4.d Hacemos el sprite del rayo invisible
            self.spriteRayo.visible = NO;
            
            // TODO 4.d Destruimos el cuerpo del rayo _bodyRayo del mundo _world con DestroyBody
            _world->DestroyBody(_bodyRayo);
            
            _bodyRayo = NULL;
        }
    }    
    
    
    // Actualización del sprite
    
    // TODO 2.c Actualizar self.spritePersonaje.position a partir de _velocidadPersonaje y dt (solo en eje x)
    self.spritePersonaje.position = ccpAdd(self.spritePersonaje.position, ccp(_velocidadPersonaje*dt, 0));
    
    
    

    // TODO 3.f Centra el viewport en la posición del personaje con centerViewport
    [self centerViewport];
    
    
    // Comprobación de colisiones
        
    // Colisiones de los asteroides
    
    for(CCSprite *asteroid in self.asteroids) {
        
        if(asteroid.visible) {
            // Colisiones disparo - asteroides
            
            CGRect bbPersonaje = [self.spritePersonaje boundingBox];
            CGRect bbRayo = [self.spriteRayo boundingBox];
            CGRect bbRoca = [asteroid boundingBox];
            
            if(asteroid.visible & CGRectIntersectsRect(bbRayo, bbRoca)) {
                
                if(asteroid.userObject==nil) {
                    asteroid.userObject = @"Exploding"; // Esto evita que el asteroide explote más de una vez
                    _score += 100;

                    // TODO 2.g Hacer que la roca sea invisible
                    //asteroid.visible = NO;

                    // TODO 2.j En lugar de hacerlo directamente invisible, reproduciremos la animación de la explosión
                                        
                    // TODO 2.j    - Para todas las acciones del asteroide (para que deje de moverse)
                    [asteroid stopAllActions];
                    
                    // TODO 2.j    - Crear una acción de animación con "animacionExpl"
                    CCAnimate *animateExplotacion = [CCAnimate actionWithAnimation:
                                                     [[CCAnimationCache sharedAnimationCache] 
                                                      animationByName:@"animacionExpl"]];

                    
                    // TODO 2.j    - Crear una acción CallBlock que haga el asteroide invisible, que ponga su propiedad userObject a nil, y que ponga en él el frame roca.png (con setDisplayFrame:)
                    
                    id blockAsteroideExpInvisible = [CCCallBlock actionWithBlock:(^{
                        asteroid.visible = NO;
                        asteroid.userObject = nil;
                        [asteroid setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"roca.png"]];
                         
                    })];
                    
                    
                    // TODO 2.j    - Crear una secuencia con las dos acciones anteriores
                    CCSequence *actionSequenceExplAstroid = [CCSequence actions: animateExplotacion, blockAsteroideExpInvisible, nil];
                    
                    // TODO 2.j    - Reproducir la secuencia en el asteroide
                    [asteroid runAction:actionSequenceExplAstroid];
                    
                    // TODO 3.j Reproducir efecto de sonido explosion.caf
                    [audio playEffect:@"explosion.caf"];

                    // TODO 3.n Actualizar etiqueta con la puntuación (self.scoreLabel.string) con formato @"Score: %05d"
                    self.scoreLabel.string = [NSString stringWithFormat:@"Score: %05d",_score];

                }
                
            }

            // Colisiones personaje - asteroides

            if(CGRectIntersectsRect(bbPersonaje, bbRoca) && self.isTouchEnabled /* TODO 2.k Intersecta personaje y asteroide y self.isTouchEnabled */) {
                
                // TODO 2.k Deshabilitar propiedad self.isTouchEnabled
                self.isTouchEnabled = NO;
                
                // TODO 2.k Detener el movimiento del personaje (llamar a deternerPersonaje)
                [self detenerPersonaje];
                
                // TODO 2.k Mostrar fotograma pers06.png
                [self.spritePersonaje setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"pers06.png"]];

                
                // TODO 2.k Crear configuración de curva de bezier (ccBezierConfig)
                ccBezierConfig bezierMuerte;
                
                // TODO 2.k   - Punto de control 1 en (self.spritePersonaje.position.x + self.spritePersonaje.contentSize.width * 2, self.spritePersonaje.position.y + self.spritePersonaje.contentSize.height)
                bezierMuerte.controlPoint_1 = ccp(self.spritePersonaje.position.x + self.spritePersonaje.contentSize.width * 2, self.spritePersonaje.position.y + self.spritePersonaje.contentSize.height);
                
                
                // TODO 2.k   - Punto de control 2 en (self.spritePersonaje.position.x + self.spritePersonaje.contentSize.width * 4, self.spritePersonaje.position.y)
                bezierMuerte.controlPoint_2 = ccp(self.spritePersonaje.position.x + self.spritePersonaje.contentSize.width * 4, self.spritePersonaje.position.y);
                
                
                // TODO 2.k   - Punto de control final en (self.spritePersonaje.position.x + self.spritePersonaje.contentSize.width * 6, -self.spritePersonaje.contentSize.height)
                bezierMuerte.endPosition = ccp(self.spritePersonaje.position.x + self.spritePersonaje.contentSize.width * 6,0 -self.spritePersonaje.contentSize.height);
                
                
                // TODO 2.k Crear acción bezier con la configuración anterior en 2 segundos
               // [self.spritePersonaje runAction:[CCBezierBy actionWithDuration:2.0 bezier:bezierMuerte]];
                
                CCMoveTo *actionBezierMuerte  = [CCBezierBy actionWithDuration:2.0 bezier:bezierMuerte];
                
                // TODO 2.k Crear acción CallBlock que haga el sprite invisible
                id blockCuerpoMuertoEnterrar = [CCCallBlock actionWithBlock:(^{
                    self.spritePersonaje.visible = NO;
                })];
                
                
                // TODO 2.k Crear acción que mueva el sprite a _respawnPosition en 2 segundos
                CCMoveTo *moverCuerpoMuerto = [CCMoveTo actionWithDuration:2.0 position:_respawnPosition];
                
                
                // TODO 2.k Crear acción CallBlock que:
                id blockEncarnacion = [CCCallBlock actionWithBlock:(^{
                    // TODO 2.k    - Haga visible el sprite
                    self.spritePersonaje.visible = YES;
                    
                    // TODO 2.k    - Muestre en el sprite el frame pers01.png
                    [self.spritePersonaje setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"pers01.png"]];

                    
                    // TODO 2.k    - Vuelva a habilitar la propiedad isTouchEnabled
                    self.isTouchEnabled = YES;
                    
                    // TODO 2.k    - Añadir esta clase como delegate a [[CCDirector sharedDirector] touchDispatcher] con addTargetedDelegate:::
                    [[[CCDirector sharedDirector] touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
                    
                })];
                
                
                // TODO 2.k Crear secuencia con las acciones anteriores
                CCSequence *actionSequenceMuerteEncarnacion = [CCSequence actions: actionBezierMuerte,blockCuerpoMuertoEnterrar,moverCuerpoMuerto ,blockEncarnacion, nil];
                
                
                // TODO 2.k Ejecutar secuencia anterior sobre el sprite
                [self.spritePersonaje runAction:actionSequenceMuerteEncarnacion];

            }
            
        } else {
            
            // Respawn asteroides
            
            // TODO 2.i Generar valores de posición inicial y final del asteroide de forma aleatoria
            
            
            // TODO 2.i  x_inicio: [0, screenSize.width]
            //NSInteger x_inicio = [self generateRandomNumberFrom:0 to:screenSize.width];
            
            // TODO 3.f Cambiaremos x_final por el ancho de todo el escenario
            NSInteger x_inicio = [self generateRandomNumberFrom:0 to:self.tiledMap.mapSize.width*self.tiledMap.tileSize.width];
            
            // TODO 2.i  x_final:  [x_inicio - asteroid.contentSize.width*3, x_inicio + asteroid.contentSize.width*3]
            NSInteger x_final = [self generateRandomNumberFrom:x_inicio - asteroid.contentSize.width*3 to:x_inicio + asteroid.contentSize.width*3];
            
                       
            // TODO 2.i  y_inicio: screenSize.height + asteroid.contentSize.height
            NSInteger y_inicio = screenSize.height + asteroid.contentSize.height;
            
            
            // TODO 2.i  y_fin:    -asteroid.contentSize.height
            NSInteger y_fin = 0 - asteroid.contentSize.height;

            
            // TODO 2.i Situar el asteroide en (x_inicio, y_inicio)
            asteroid.position = ccp(x_inicio, y_inicio);
            
            
            // TODO 2.i Hacer que el asteroide sea visible
            asteroid.visible = YES;
            
            
            // TODO 2.i Programar acción que mueva el asteroide hasta (x_fin, y_fin)
            CCMoveTo *moveAsteroidTo = [CCMoveTo actionWithDuration:4.0 position:ccp(x_final,y_fin)];
            
            // TODO 2.i Programar acción que vuelva a poner el asteroide invisible
            id blockAsteroideInvisible = [CCCallBlock actionWithBlock:(^{
                asteroid.visible = NO;
            })];
            
            // TODO 2.i Crear secuencia con las dos acciones anteriores
            CCSequence *actionSequenceAstroid = [CCSequence actions: moveAsteroidTo, blockAsteroideInvisible, nil];
            
            // TODO 2.i Ejecutar la acción en el asteroide
            [asteroid runAction:actionSequenceAstroid];
            
        }
        
        
        // Colisiones sprite - limites del escenario
        
        // TODO 3.g Obtenemos las coordenadas del tile correspondientes a la posición del sprite (con tileCoordForPosition)
        CGPoint currentTilepos = [self tileCoordForPosition:self.spritePersonaje.position];
        
        // TODO 3.g Obtenemos el tile a la izquierda del actual
        CGPoint tileIzq = ccp(currentTilepos.x-1,currentTilepos.y);
        
        // TODO 3.g Obtenemos el tile a la derecha del actual
        CGPoint tileDert = ccp(currentTilepos.x+1,currentTilepos.y);
        
        
        // TODO 3.g Para el tile izquierdo y derecho hacemos, comprobamos si son colisionables, en tal caso:
        
        if ([self isCollidableTileAt:tileIzq])
        {
        // TODO 3.g   - Obtenemos rectángulo que ocupa el tile (con rectForTileAt:)
            CGRect tileLeftRect = [self rectForTileAt:tileIzq];
            
        // TODO 3.g   - Comprobamos si dicho rectángulo intersecta con nuestro personaje. En tal caso:
            if(CGRectIntersectsRect(tileLeftRect, self.spritePersonaje.boundingBox))
            {
        // TODO 3.g     - Detenemos el personaje (detenerPersonaje)
                [self detenerPersonaje];
        // TODO 3.g     - Situamos al personaje a la izquierda o derecha del tile, sin solaparse con él
                self.spritePersonaje.position = ccp(tileLeftRect.origin.x + 
                                                    tileLeftRect.size.width + 
                                                    self.spritePersonaje.contentSize.width/2, 
                                                    self.tiledMap.tileSize.height + 
                                                    self.spritePersonaje.contentSize.height/2);
            }
        }
        
        if ([self isCollidableTileAt:tileDert])
        {
            // TODO 3.g   - Obtenemos rectángulo que ocupa el tile (con rectForTileAt:)
            CGRect tileRightRect = [self rectForTileAt:tileDert];
            
            // TODO 3.g   - Comprobamos si dicho rectángulo intersecta con nuestro personaje. En tal caso:
            if(CGRectIntersectsRect(tileRightRect, self.spritePersonaje.boundingBox))
            {
                // TODO 3.g     - Detenemos el personaje (detenerPersonaje)
                [self detenerPersonaje];
                // TODO 3.g     - Situamos al personaje a la izquierda o derecha del tile, sin solaparse con él
                
                self.spritePersonaje.position = ccp(tileRightRect.origin.x - 
                                                    self.spritePersonaje.contentSize.width/2, 
                                                    self.tiledMap.tileSize.height + 
                                                    self.spritePersonaje.contentSize.height/2);
            
            }
        }     
    }
}



#pragma mark - Eventos de la pantalla tactil

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    CGPoint location = [self convertTouchToNodeSpace: touch];
    CGSize screenSize = [[CCDirector sharedDirector] winSize];

    if(location.y > screenSize.height / 2) {
        [self disparar];
    } else if(location.x > self.spritePersonaje.position.x) {
        [self moverPersonajeDerecha];
    } else if(location.x < self.spritePersonaje.position.x) {
        [self moverPersonajeIzquierda];
    }
       
    return YES;
}

- (void)ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event {
    [self detenerPersonaje];
}

- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
    [self detenerPersonaje];
}

- (void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {

}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	[_spritePersonaje release];
    [_spriteRayo release];
    [_asteroids release];
    [_scoreLabel release];
    [_tiledMap release];

    delete _world;
    _world = NULL;
    
	// don't forget to call "super dealloc"
	[super dealloc];
}


@end
