//
//  Libro.m
//  BibliotecaJTech
//
//  Created by Lion User on 16/01/2013.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "Libro.h"


@implementation Libro

@dynamic isbn;
@dynamic titulo;
@dynamic autor;
@dynamic fecha;
@dynamic paginas;
@dynamic imagen;
@dynamic puntuacion;
@dynamic estado;


@end
