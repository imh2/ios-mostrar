//
//  UANuevoLibroViewController.h
//  BibliotecaJTech
//
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UALibro;

@interface UANuevoLibroViewController : UITableViewController<UIAlertViewDelegate>

@property(nonatomic,weak) UALibro *libro;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButton;

@property(nonatomic,weak) IBOutlet UILabel *labelTitulo;
@property(nonatomic,weak) IBOutlet UILabel *labelAutor;
@property(nonatomic,weak) IBOutlet UIImageView *imagePortada;
@property(nonatomic,weak) IBOutlet UILabel *labelIsbn;
@property(nonatomic,weak) IBOutlet UILabel *labelPaginas;
@property(nonatomic,weak) IBOutlet UILabel *labelFecha;

- (IBAction) agregarLibro:(id)sender;

@end
