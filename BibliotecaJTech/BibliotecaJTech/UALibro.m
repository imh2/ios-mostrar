//
//  Libro.m
//  Biblioteca
//


#import "UALibro.h"

@implementation UALibro

@synthesize isbn = _isbn;
@synthesize titulo = _titulo;
@synthesize autor = _autor;
@synthesize imagen = _imagen;
@synthesize fecha = _fecha;
@synthesize paginas = _paginas;
@synthesize puntuacion = _puntuacion;
@synthesize estado = _estado;
@synthesize imagenPortada = _imagenPortada;

- (id)init
{
    self = [super init];
    if (self) {
        _puntuacion = 5;
        _estado = UAEstadoLibroSinLeer;
        _fecha = [NSDate date];
    }
    
    return self;
}


@end
