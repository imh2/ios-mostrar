//
//  UAAgregarViewController.m
//  BibliotecaJTech
//
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "UAAgregarViewController.h"

#import "UAFuenteDatosCoreData.h"
#import "UALibro.h"
#import "UANuevoLibroViewController.h"
#import "ImageDownloader.h"


@interface UAAgregarViewController() 

- (void) reloadLibrosFiltrados;

@end


@implementation UAAgregarViewController

@synthesize libros = _libros;
@synthesize librosFiltrados = _librosFiltrados;
@synthesize currentConnection = _currentConnection;
@synthesize downloadedData = _downloadedData;
@synthesize downloadingImages = _downloadingImages;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.libros = [[NSMutableArray alloc]init];
    
    NSURL *url = [NSURL URLWithString:@"http://server.jtech.ua.es:80/jbib-rest/resources/libros"];
    
    //NSURLRequest *theRequest = [NSURLRequest requestWithURL: url];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL: url];
    
    [theRequest setValue:@"application/xml" forHTTPHeaderField:@"Accept"];
    
    self.currentConnection = [NSURLConnection connectionWithRequest: theRequest delegate: self];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    // Inicializamos el diccionario
    self.downloadingImages = [[NSMutableDictionary alloc] init];
    

    //self.libros = [[UAFuenteDatosCoreData sharedFuenteDatos] librosBiblioteca];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Storyboard

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {    

    UANuevoLibroViewController *controller = segue.destinationViewController;
    
    if(sender==self.searchDisplayController.searchResultsTableView) {
        controller.libro = [_librosFiltrados objectAtIndex: self.searchDisplayController.searchResultsTableView.indexPathForSelectedRow.row];        
    } else {
        controller.libro = [_libros objectAtIndex: self.tableView.indexPathForSelectedRow.row];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(tableView == self.searchDisplayController.searchResultsTableView) {
        return [_librosFiltrados count];
    } else {
        return [_libros count];        
    }
}

- (void) cargarImagen: (UALibro *) libro 
{
    if([self.downloadingImages objectForKey: libro.isbn] == nil) {
        NSString *urlString = [NSString
                               stringWithFormat:
                               @"http://server.jtech.ua.es:80/jbib-rest/resources/libros/%@/imagen", 
                               libro.isbn];
        
        ImageDownloader *theDownloader = [[ImageDownloader alloc] 
                                          initWithUrl:urlString 
                                          object:libro 
                                          target:self                                                      
                                          selector: @selector(imagenCargada:)];
        
        [self.downloadingImages setValue: theDownloader forKey: libro.isbn];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CeldaLibro";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    UALibro *libro = nil;
    if(tableView == self.searchDisplayController.searchResultsTableView) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        libro = [_librosFiltrados objectAtIndex: indexPath.row];
    } else {
        libro = [_libros objectAtIndex: indexPath.row];
    }    
    
    cell.textLabel.text = libro.titulo;
    cell.detailTextLabel.text = libro.autor;
    cell.imageView.image = [UIImage imageNamed:libro.imagen];
    
    if (libro.imagenPortada == nil){
        cell.imageView.image = [UIImage imageNamed:@"Placeholder.png"];
        
        // Carga de la imagen
        [self cargarImagen:libro]; 
    }
    else {
        cell.imageView.image = libro.imagenPortada;
    }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"segueNuevoLibro" sender: tableView];
}

#pragma mark - Search display delegate

- (void)reloadLibrosFiltrados {
    if(self.searchDisplayController.searchBar.selectedScopeButtonIndex==0) {
        self.librosFiltrados = [[UAFuenteDatosCoreData sharedFuenteDatos] buscaLibrosPorTitulo: self.searchDisplayController.searchBar.text];
    } else {
        self.librosFiltrados = [[UAFuenteDatosCoreData sharedFuenteDatos] buscaLibrosPorIsbn: self.searchDisplayController.searchBar.text];        
    }
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    [self reloadLibrosFiltrados];
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    [self reloadLibrosFiltrados];
    return YES;    
}

#pragma mark - Eventos de la conexion al servicio

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    if(response.expectedContentLength != NSURLResponseUnknownLength) {
        self.downloadedData = [NSMutableData dataWithCapacity: response.expectedContentLength];
        self.libros = [NSMutableData dataWithCapacity: response.expectedContentLength];
    } else {
        self.downloadedData = [NSMutableData data];
        
    }
}


- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data 
{    
    
    // TODO: Añadimos el data al objeto downloadedData
    
    [self.downloadedData appendData:data];
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    self.currentConnection = nil;
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection 
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    // TODO: Parseamos el objeto JSON que tenemos en self.downloadedData y recorremos 
    // todos los libros añadiéndolos al array de libros de la clase

   /* 
    NSError *error = nil;
    
    NSArray *mensajes = [NSJSONSerialization JSONObjectWithData: self.downloadedData 
                                                        options:0 
                                                          error:&error];
    if(error==nil) {
        for(NSDictionary *mensaje in mensajes) {
            
            NSLog(@" %@ *** ",mensaje);
            
            NSString *autor = [mensaje valueForKey:@"autor"];
            NSString *isbn = [mensaje valueForKey:@"isbn"];
            NSNumber *numPaginas = [mensaje valueForKey:@"numPaginas"];
            NSString *titulo = [mensaje valueForKey:@"titulo"];

            UALibro *miLibro = [[UALibro alloc] init];
            
            miLibro.autor = autor;
            miLibro.isbn = isbn;
            miLibro.paginas = numPaginas.integerValue;
            miLibro.titulo = titulo;
            
            [self.libros addObject:miLibro] ;
             
        }
    
    }
    */
    
NSXMLParser *parser = [[NSXMLParser alloc] initWithData: self.downloadedData];

parser.delegate = self;

[parser parse];

[self.tableView reloadData];
}


#pragma mark - NSXMLParser delegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    if([[elementName lowercaseString] isEqualToString:@"libro"]) {
        
        UALibro *miLibro = [[UALibro alloc] init];
        
        miLibro.autor = [attributeDict objectForKey:@"autor"];
        miLibro.isbn = [attributeDict objectForKey:@"isbn"];
        miLibro.titulo = [attributeDict objectForKey:@"titulo"];
        NSNumber *numPaginas = [attributeDict objectForKey:@"numPaginas"];
        miLibro.paginas = numPaginas.integerValue;
        
        [self.libros addObject:miLibro];
        
    }
    
}




- (void) imagenCargada: (id) sender {
    UALibro *libro = [sender object];   
    libro.imagenPortada = [sender image];
    
    [self.tableView reloadData];
    
}



@end
