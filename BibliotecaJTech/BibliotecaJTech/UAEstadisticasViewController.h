//
//  UAFirstViewController.h
//  BibliotecaJTech
//
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAEstadisticasViewController : UITableViewController

@property(nonatomic, weak) IBOutlet UILabel *labelNumeroLibros;
@property(nonatomic, weak) IBOutlet UILabel *labelNumeroLibrosLeidos;
@property(nonatomic, weak) IBOutlet UILabel *labelPuntuacionMedia;


@end
