//
//  UADatosLibroViewController.m
//  BibliotecaJTech
//
//

#import <Twitter/Twitter.h>
#import <Accounts/Accounts.h>

#import "UADatosLibroViewController.h"

#import "UALibro.h"
#import "UAFuenteDatosCoreData.h"

@implementation UADatosLibroViewController

@synthesize labelAutor = _labelAutor;
@synthesize labelEstado = _labelEstado;
@synthesize labelTitulo = _labelTitulo;
@synthesize labelPaginas = _labelPaginas;
@synthesize labelPuntuacion = _labelPuntuacion;
@synthesize imagePortada = _imagePortada;
@synthesize stepperPuntuacion = _stepperPuntuacion;

@synthesize libro = _libro;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        

    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIBarButtonItem *tweetBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Twitter" style:UIBarButtonItemStylePlain target:self action:@selector(publicarEnTwitter:)];
    
    self.navigationItem.rightBarButtonItem = tweetBarButton;
}

- (void)viewWillAppear:(BOOL)animated {
    self.title = self.libro.titulo;
    
    self.labelTitulo.text = self.libro.titulo;
    self.labelAutor.text = self.libro.autor;
    self.labelPaginas.text = [NSString stringWithFormat:@"%d páginas", self.libro.paginas];
    self.labelEstado.text = [[[UAFuenteDatosCoreData sharedFuenteDatos] cadenaEstadoLibro] objectAtIndex: self.libro.estado];
    self.labelPuntuacion.text = [NSString stringWithFormat: @"%d", self.libro.puntuacion];
    self.stepperPuntuacion.value=self.libro.puntuacion ;
    self.imagePortada.image = self.libro.imagenPortada;  
}


-(void) publicarEnTwitter: (id) sender
{
    NSString *urlString = [NSString
                           stringWithFormat:
                           @"http://server.jtech.ua.es:80/jbib-rest/resources/libros/%@/imagen",
                           self.libro.isbn];
    TWTweetComposeViewController *controller = [[TWTweetComposeViewController alloc] init];
    [controller setInitialText: self.libro.titulo];
    [controller addImage:self.libro.imagenPortada];
    [controller addURL:urlString];
    [controller setCompletionHandler:
     ^(TWTweetComposeViewControllerResult result) {
         switch(result) {
             case TWTweetComposeViewControllerResultDone:
                 NSLog(@"Tweet enviado");
                 break;
             case TWTweetComposeViewControllerResultCancelled:
                 NSLog(@"Tweet cancelado");
                 break;
         }
         [self dismissModalViewControllerAnimated:YES];
     }];
    [self presentModalViewController:controller animated: YES];

}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Eventos

- (IBAction) puntuacionModificada:(id)sender {
    self.libro.puntuacion = self.stepperPuntuacion.value;
    self.labelPuntuacion.text = [NSString stringWithFormat: @"%d", self.libro.puntuacion];
    [[UAFuenteDatosCoreData sharedFuenteDatos] actualizaLibro:self.libro]; 
}


@end
