//
//  UAInicioViewController.h
//  BibliotecaJTech
//
//   Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAInicioViewController : UIViewController

@property(nonatomic,weak) IBOutlet UILabel *labelTitulo;
@property(nonatomic,weak) IBOutlet UIImageView *imageCartel;

@end
