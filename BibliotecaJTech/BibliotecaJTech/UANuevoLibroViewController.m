//
//  UANuevoLibroViewController.m
//  BibliotecaJTech
//
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "UANuevoLibroViewController.h"

#import "UALibro.h"
#import "UAFuenteDatosCoreData.h"

@implementation UANuevoLibroViewController

@synthesize libro = _libro;
@synthesize barButton = _barButton;

@synthesize labelIsbn = _labelIsbn;
@synthesize labelFecha = _labelFecha;
@synthesize labelPaginas = _labelPaginas;
@synthesize imagePortada = _imagePortada;
@synthesize labelAutor = _labelAutor;
@synthesize labelTitulo = _labelTitulo;



- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.title = _libro.titulo;
    
    self.labelPaginas.text = [NSString stringWithFormat: @"%d", _libro.paginas];
    self.labelIsbn.text = _libro.isbn;
    self.labelFecha.text =  [NSDateFormatter localizedStringFromDate:_libro.fecha dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];
    self.imagePortada.image = _libro.imagenPortada;
    self.labelTitulo.text = _libro.titulo;
    self.labelAutor.text = _libro.autor;
}

- (void)viewDidUnload
{
    [self setBarButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if([[UAFuenteDatosCoreData sharedFuenteDatos] existeLibroEnColeccionConIsbn: _libro.isbn]) {
        self.barButton.enabled = NO;
        self.barButton.title = @"En colección";
    } else {
        self.barButton.enabled = YES;
        self.barButton.title = @"Agregar";
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Alert view delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 1) {
        [[[UAFuenteDatosCoreData sharedFuenteDatos] librosColeccion] addObject: self.libro];
        
        [[UAFuenteDatosCoreData sharedFuenteDatos] agregaLibroAColeccion:self.libro];
        
        self.barButton.enabled = NO;
        self.barButton.title = @"En colección";
    
    }
}

#pragma mark - Acciones

- (IBAction) agregarLibro:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Agregar a colección" message:[NSString stringWithFormat:@"¿TRUNCO Desea agregar el libro '%@' a su colección?", self.libro.titulo] delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Si", nil];
    [alert show];    
}



@end
