//
//  UAEstadoLibroViewController.h
//  BibliotecaJTech
//


#import <UIKit/UIKit.h>

//#import "UAColeccionDelegate.h"

@class UALibro;

@interface UAEstadoLibroViewController : UITableViewController

@property (nonatomic,weak) NSArray *items;
@property (nonatomic,weak) UALibro *libro;

@end
