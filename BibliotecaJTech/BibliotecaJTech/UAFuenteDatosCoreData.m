//
//  UAFuenteDatosCoreData.m
//  BibliotecaJTech
//


#import "UAFuenteDatosCoreData.h"
#import "UALibro.h"
#import "UAAppDelegate.h"
#import "Libro.h"

@implementation UAFuenteDatosCoreData


@synthesize librosBiblioteca = _librosBiblioteca;
@synthesize librosColeccion = _librosColeccion;

@synthesize cadenaEstadoLibro = _cadenaEstadoLibro;
@synthesize fetchedResultsController = __fetchedResultsController;
@synthesize managedObjectContext = __managedObjectContext;

+ (id) sharedFuenteDatos 
{
    static UAFuenteDatosCoreData *_UAFuenteDatosCoreData = nil;
    if(_UAFuenteDatosCoreData == nil) {
        _UAFuenteDatosCoreData = [[UAFuenteDatosCoreData alloc] init];
    }
    return _UAFuenteDatosCoreData;
}

- (id)init
{
    self = [super init];
    if (self) {
        // Inicializa libros
        _librosBiblioteca = [[NSMutableArray alloc] initWithCapacity:12];
        _librosColeccion = [[NSMutableArray alloc] initWithCapacity:12];
        
        _cadenaEstadoLibro = [NSArray arrayWithObjects: @"No he empezado", @"Lo estoy leyendo", @"Lo he leído", nil];
        
        
        UAAppDelegate *delegate = (UAAppDelegate *)[[UIApplication sharedApplication] delegate];
        self.managedObjectContext = delegate.managedObjectContext;
        
        UALibro *libro = nil;
        
        libro = [[UALibro alloc] init];
        libro.isbn = @"0131401572";
        libro.titulo = @"Data Access Patterns";
        libro.autor = @"Clifton Nock";
        libro.imagen = [NSString stringWithFormat:@"%@.jpg", libro.isbn];
        libro.paginas = 512;
        [_librosBiblioteca addObject: libro];
        
        libro = [[UALibro alloc] init];
        libro.isbn = @"0321127420";
        libro.titulo = @"Patterns Of Enterprise Application Architecture";
        libro.autor = @"Martin Fowler";
        libro.imagen = [NSString stringWithFormat:@"%@.jpg", libro.isbn];
        libro.paginas = 533;
        [_librosBiblioteca addObject: libro];
        
        libro = [[UALibro alloc] init];
        libro.isbn = @"0321180860";
        libro.titulo = @"Understanding SOA with Web Services";
        libro.autor = @"Eric Newcomer and Greg Lomow";
        libro.imagen = [NSString stringWithFormat:@"%@.jpg", libro.isbn];
        libro.paginas = 465;
        [_librosBiblioteca addObject: libro];
        
        libro = [[UALibro alloc] init];
        libro.isbn = @"0321278658";
        libro.titulo = @"Extreme Programming Explained - Embrace Change";
        libro.autor = @"Kent Beck";
        libro.imagen = [NSString stringWithFormat:@"%@.jpg", libro.isbn];
        libro.paginas = 189;
        [_librosBiblioteca addObject: libro];
        
        libro = [[UALibro alloc] init];
        libro.isbn = @"0321482751";
        libro.titulo = @"Agile Software Development";
        libro.autor = @"Alistair Cockburn";
        libro.imagen = [NSString stringWithFormat:@"%@.jpg", libro.isbn];
        libro.paginas = 467;
        [_librosBiblioteca addObject: libro];
        
        libro = [[UALibro alloc] init];
        libro.isbn = @"0471768944";
        libro.titulo = @"Service-Oriented Architecture (SOA)";
        libro.autor = @"Eric A. Marks and Michael Bell";
        libro.imagen = [NSString stringWithFormat:@"%@.jpg", libro.isbn];
        libro.paginas = 276;
        [_librosBiblioteca addObject: libro];
        
        libro = [[UALibro alloc] init];
        libro.isbn = @"0764558315";
        libro.titulo = @"Expert One-On-One J2EE Development Without EJB";
        libro.autor = @"Rod Johnson";
        libro.imagen = [NSString stringWithFormat:@"%@.jpg", libro.isbn];
        libro.paginas = 576;
        [_librosBiblioteca addObject: libro];
        
        libro = [[UALibro alloc] init];
        libro.isbn = @"097451408X";
        libro.titulo = @"Practices of an Agile Developer";
        libro.autor = @"Venkat Subramaniam and Andy Hunt";
        libro.imagen = [NSString stringWithFormat:@"%@.jpg", libro.isbn];
        libro.paginas = 208;
        [_librosBiblioteca addObject: libro];
        
        libro = [[UALibro alloc] init];
        libro.isbn = @"0977616649";
        libro.titulo = @"Agile Retrospectives";
        libro.autor = @"Esther Derby and Diana Larsen";
        libro.imagen = [NSString stringWithFormat:@"%@.jpg", libro.isbn];
        libro.paginas = 200;
        [_librosBiblioteca addObject: libro];
        
        libro = [[UALibro alloc] init];
        libro.isbn = @"1590595874";
        libro.titulo = @"Beginning GIMP";
        libro.autor = @"Akkana Peck";
        libro.imagen = [NSString stringWithFormat:@"%@.jpg", libro.isbn];
        libro.paginas = 528;
        [_librosBiblioteca addObject: libro];
        
        libro = [[UALibro alloc] init];
        libro.isbn = @"1932394885";
        libro.titulo = @"Java Persistence with Hibernate";
        libro.autor = @"Christian Bauer and Gavin King";
        libro.imagen = [NSString stringWithFormat:@"%@.jpg", libro.isbn];
        libro.paginas = 841;
        [_librosBiblioteca addObject: libro];
        
        libro = [[UALibro alloc] init];
        libro.isbn = @"1933988347";
        libro.titulo = @"EJB 3 In Action";
        libro.autor = @"Debu Panda";
        libro.imagen = [NSString stringWithFormat:@"%@.jpg", libro.isbn];
        libro.paginas = 677;
        [_librosBiblioteca addObject: libro];
    }
    
    return self;
}

- (NSArray *) buscaLibrosPorIsbn: (NSString *) isbn 
{
    NSMutableArray *libros = [NSMutableArray arrayWithCapacity: [_librosBiblioteca count]];
    for(UALibro *libro in _librosBiblioteca) {
        NSRange range = [libro.isbn rangeOfString: isbn];
        if(range.location != NSNotFound) {
            [libros addObject: libro];
        }
    }
    
    return libros;
}


- (NSArray *) buscaLibrosPorTitulo: (NSString *) titulo 
{
    NSMutableArray *libros = [NSMutableArray arrayWithCapacity: [_librosBiblioteca count]];
    for(UALibro *libro in _librosBiblioteca) {
        NSRange range = [libro.titulo rangeOfString: titulo];
        if(range.location != NSNotFound) {
            [libros addObject: libro];
        }
    }
    
    return libros;
}

- (BOOL) existeLibroEnColeccionConIsbn: (NSString*) isbn {
    for(UALibro *libro in _librosColeccion) {
        if([libro.isbn isEqualToString: isbn]) {
            return YES;
        }
    }
    
    return NO;
}

- (CGFloat) porcentajeLibrosLeidos {
    NSUInteger librosLeidos = 0;
    
    if([_librosColeccion count] == 0) {
        return 0.0;
    }
    
    for(UALibro *libro in _librosColeccion) {
        if(libro.estado == UAEstadoLibroLeido) {
            librosLeidos++;
        }
    }
    
    return (CGFloat)100.0 * librosLeidos / [_librosColeccion count];
}

- (CGFloat) notaMediaLibrosColeccion {
    CGFloat sumaNotas = 0.0;
    
    if([_librosColeccion count] == 0) {
        return 0.0;
    }
    
    for(UALibro *libro in _librosColeccion) {
        sumaNotas += libro.puntuacion;
    }
    
    return sumaNotas / [_librosColeccion count];
}

- (UALibro *) libroAleatorio {
    NSUInteger numLibros = [_librosBiblioteca count];
    
    if(numLibros == 0) {
        return nil;
    } else {
        int i = arc4random() % numLibros;
        return [_librosBiblioteca objectAtIndex: i];
    }
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (__fetchedResultsController != nil) {
        return __fetchedResultsController;
    }
    
    
    // Set up the fetched results controller.
    // Create the fetch request for the entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Libro"
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"titulo"
                                                                   ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = 
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest 
                                        managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    aFetchedResultsController.delegate = self;
    
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return __fetchedResultsController;
}

-(void) actualizaLibrosColeccion {
    
    for (Libro *libroManagedObject in self.fetchedResultsController.fetchedObjects){
        NSLog(@"Cargo libro a mi coleccion: %@",libroManagedObject.titulo);
        
        UALibro *libro = nil;
        
        libro = [[UALibro alloc] init];
        libro.isbn = libroManagedObject.isbn;
        libro.titulo = libroManagedObject.titulo;
        libro.autor = libroManagedObject.autor;
        libro.imagen = libroManagedObject.imagen;
        libro.paginas = [libroManagedObject.paginas intValue];
        libro.estado = [libroManagedObject.estado intValue];
        libro.puntuacion = [libroManagedObject.puntuacion intValue];
        
        [_librosColeccion addObject:libro];
    }

}


// Borra un libro de la coleccion
-(void ) borraLibroDeColeccion:(UALibro *) libro {
    
    // Actualizamos el contexto para que se recargue el listado de libros
    self.fetchedResultsController = nil;
    
    NSManagedObjectContext *context = [self.fetchedResultsController
                                       managedObjectContext];    
    
    // Recorremos cada uno de los libros de la coleccion y lo comparamos con el que 
    // queremos borrar. Una vez que lo encontramos lo borraremos del contexto (Core Data)
    for (Libro *libroManagedObject in self.fetchedResultsController.fetchedObjects){
        
        if ([libroManagedObject.isbn isEqualToString:libro.isbn]) {
            
            [context deleteObject:libroManagedObject];
            
            // Save the context.
            NSError *error = nil;
            if (![context save:&error]) {
                
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
            }
            
            
            NSLog(@"Borro libro: %@", libro.titulo);
            
            
            break;
        }
    }
    
    
}

-(void) agregaLibroAColeccion:(UALibro *) libro{
    
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    
    Libro *newColeccionLibro = [NSEntityDescription
                    insertNewObjectForEntityForName:@"Libro"
                    inManagedObjectContext:context];
    
    
    newColeccionLibro.titulo = libro.titulo;
    newColeccionLibro.isbn = libro.isbn;
    newColeccionLibro.autor = libro.autor;
    newColeccionLibro.imagen = libro.imagen;
    newColeccionLibro.paginas = [NSNumber numberWithInt:libro.paginas];
    newColeccionLibro.estado = [NSNumber numberWithInt:libro.estado];
    newColeccionLibro.puntuacion = [NSNumber numberWithInt:libro.puntuacion];
    
     
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Que has hecho trunco ??? error %@, %@", error, [error userInfo]);
        abort();
    }

}

-(void) actualizaLibro:(UALibro *)libro{
    // Actualizamos el contexto para que se recargue el listado de libros
    self.fetchedResultsController = nil;
    
    NSManagedObjectContext *context = [self.fetchedResultsController
                                       managedObjectContext];    
    
    // Recorremos cada uno de los libros de la coleccion y lo comparamos con el que 
    // queremos borrar. Una vez que lo encontramos lo borraremos del contexto (Core Data)
    for (Libro *libroManagedObject in self.fetchedResultsController.fetchedObjects){
        
        if ([libroManagedObject.isbn isEqualToString:libro.isbn]) {
            
            libroManagedObject.puntuacion = [NSNumber numberWithInt:libro.puntuacion];
            libroManagedObject.estado = [NSNumber numberWithInt:libro.estado];
            
            // Save the context.
            NSError *error = nil;
            if (![context save:&error]) {
                
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
            }
            
            
            NSLog(@"Actualiza libro: %@", libro.titulo);
            
            
            break;
        }
    }

}


@end
