//
//  UAFirstViewController.m
//  BibliotecaJTech
//
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "UAEstadisticasViewController.h"

#import "UAFuenteDatosCoreData.h"

@implementation UAEstadisticasViewController

@synthesize labelNumeroLibros = _labelNumeroLibros;
@synthesize labelNumeroLibrosLeidos = _labelNumeroLibrosLeidos;
@synthesize labelPuntuacionMedia = _labelPuntuacionMedia;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSUInteger numLibros = [[[UAFuenteDatosCoreData sharedFuenteDatos] librosColeccion] count];
    CGFloat notaMedia = [[UAFuenteDatosCoreData sharedFuenteDatos] notaMediaLibrosColeccion];
    CGFloat porcentajeLeidos = [[UAFuenteDatosCoreData sharedFuenteDatos] porcentajeLibrosLeidos];
    
    self.labelNumeroLibros.text = [NSString stringWithFormat:@"%d", numLibros];
    self.labelNumeroLibrosLeidos.text = [NSString stringWithFormat:@"%.2f %%", porcentajeLeidos];
    self.labelPuntuacionMedia.text = [NSString stringWithFormat:@"%.2f", notaMedia];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

@end
