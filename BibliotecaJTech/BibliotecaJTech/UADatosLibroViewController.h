//
//  UADatosLibroViewController.h
//  BibliotecaJTech
//
//

#import <UIKit/UIKit.h>


@class UALibro;

@interface UADatosLibroViewController : UITableViewController

@property(nonatomic,weak) UALibro *libro;

@property(nonatomic,weak) IBOutlet UILabel *labelTitulo;
@property(nonatomic,weak) IBOutlet UILabel *labelAutor;
@property(nonatomic,weak) IBOutlet UILabel *labelPaginas;
@property(nonatomic,weak) IBOutlet UIImageView *imagePortada;
@property(nonatomic,weak) IBOutlet UILabel *labelEstado;
@property(nonatomic,weak) IBOutlet UILabel *labelPuntuacion;
@property(nonatomic,weak) IBOutlet UIStepper *stepperPuntuacion;

- (IBAction) puntuacionModificada:(id)sender;

@end
