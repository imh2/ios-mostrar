//
//  ImageDownloader.m
//  Biblioteca
//
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ImageDownloader.h"

@implementation ImageDownloader

@synthesize connection = _connection;
@synthesize data = _data;
@synthesize url = _url;
@synthesize image = _image;

@synthesize target = _target;
@synthesize selector = _selector;
@synthesize object = _object;

- (id)initWithUrl: (NSString *) url object: (id) object target: (id) target selector: (SEL) selector
{
    self = [super init];
    if (self) {
        NSURL *urlImagen = [NSURL URLWithString: url];
        NSURLRequest *theRequest = [NSURLRequest requestWithURL: urlImagen];
        NSURLConnection *theConnection = [NSURLConnection connectionWithRequest: theRequest delegate: self];        
        
        self.target = target;
        self.selector = selector;
        self.url = url;

        self.object = object;        
        self.connection = theConnection;
        self.data = [NSMutableData data];
    }
    
    return self;
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data 
{    
    [self.data appendData: data];
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection 
{
    UIImage *theImage = [UIImage imageWithData: self.data];
    self.image = theImage;
    
    [self.target performSelector: self.selector withObject: self];
}


@end
