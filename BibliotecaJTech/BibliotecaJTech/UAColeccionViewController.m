//
//  UAColeccionViewController.m
//  BibliotecaJTech
//
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "UAColeccionViewController.h"

#import "UAFuenteDatosCoreData.h"
#import "UALibro.h"
#import "UADatosLibroViewController.h"

#import "ImageDownloader.h"



@implementation UAColeccionViewController

@synthesize libros = _libros;

@synthesize downloadingImages = _downloadingImages;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [[UAFuenteDatosCoreData sharedFuenteDatos] actualizaLibrosColeccion];
    
    self.libros = [[UAFuenteDatosCoreData sharedFuenteDatos] librosColeccion];
    
    
    
    // Inicializamos el diccionario
    self.downloadingImages = [[NSMutableDictionary alloc] init];
   
}





- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];    

    [self.tableView reloadData];    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.libros count];        
}


- (void) cargarImagen: (UALibro *) libro 
{
    if([self.downloadingImages objectForKey: libro.isbn] == nil) {
        NSString *urlString = [NSString
                               stringWithFormat:
                               @"http://server.jtech.ua.es:80/jbib-rest/resources/libros/%@/imagen", 
                               libro.isbn];
        
        ImageDownloader *theDownloader = [[ImageDownloader alloc] 
                                          initWithUrl:urlString 
                                          object:libro 
                                          target:self                                                      
                                          selector: @selector(imagenCargada:)];
        
        [self.downloadingImages setValue: theDownloader forKey: libro.isbn];
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CeldaLibro";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }        
    
    // Configure the cell...
    UALibro *libro = [_libros objectAtIndex: indexPath.row];
    cell.textLabel.text = libro.titulo;
    cell.detailTextLabel.text = libro.autor;
   // cell.imageView.image = [UIImage imageNamed:libro.imagen] ;
    
    
    if (libro.imagenPortada == nil){
        cell.imageView.image = [UIImage imageNamed:@"Placeholder.png"];
        
        // Carga de la imagen
        [self cargarImagen:libro]; 
    }
    else {
        cell.imageView.image = libro.imagenPortada;
    }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        
        
        UALibro *delteLibro = [self.libros objectAtIndex:indexPath.row ];
        
        [self.libros removeObjectAtIndex: indexPath.row];
        
        [[UAFuenteDatosCoreData sharedFuenteDatos] borraLibroDeColeccion:delteLibro];

        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];        
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Storyboard

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    UADatosLibroViewController *datosController = segue.destinationViewController;
    datosController.libro = [self.libros objectAtIndex: self.tableView.indexPathForSelectedRow.row];
}

- (void) imagenCargada: (id) sender {
    UALibro *libro = [sender object];   
    libro.imagenPortada = [sender image];
    
    [self.tableView reloadData];
    
}

@end
