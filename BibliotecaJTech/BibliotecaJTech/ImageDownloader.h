//
//  ImageDownloader.h
//  Biblioteca
//
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageDownloader : NSObject

- (id)initWithUrl: (NSString *) url object: (id) object target: (id) target selector: (SEL) selector;

@property(nonatomic,retain) NSString *url;
@property(nonatomic,retain) NSURLConnection *connection;
@property(nonatomic,retain) NSMutableData *data;
@property(nonatomic,retain) UIImage *image;

@property(nonatomic,assign) id target;
@property(nonatomic,assign) SEL selector;
@property(nonatomic,assign) id object;

@end
