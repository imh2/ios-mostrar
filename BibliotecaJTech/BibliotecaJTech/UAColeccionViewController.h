//
//  UAColeccionViewController.h
//  BibliotecaJTech
//
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UAColeccionViewController : UITableViewController

@property(nonatomic,weak) NSMutableArray *libros;


@property(nonatomic,retain) NSMutableDictionary *downloadingImages; //imagenes descargadas

@end
