//
//  UAFuenteDatosCoreData.h
//  BibliotecaJTech
//


#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class UALibro;

@interface UAFuenteDatosCoreData : NSObject


+ (id) sharedFuenteDatos;
- (NSArray *) buscaLibrosPorTitulo: (NSString *) titulo;
- (NSArray *) buscaLibrosPorIsbn: (NSString *) isbn;
- (BOOL) existeLibroEnColeccionConIsbn: (NSString*) isbn;
- (CGFloat) porcentajeLibrosLeidos;
- (CGFloat) notaMediaLibrosColeccion;
- (UALibro *) libroAleatorio;

@property (nonatomic, readonly, strong) NSMutableArray *librosBiblioteca; 
@property (nonatomic, readonly, strong) NSMutableArray *librosColeccion; 

@property (nonatomic, readonly, strong) NSArray *cadenaEstadoLibro;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

-(void) actualizaLibrosColeccion;
-(void) agregaLibroAColeccion:(UALibro *) libro;
-(void) borraLibroDeColeccion:(UALibro *) libro;
-(void) actualizaLibro:(UALibro *)libro;


@end
