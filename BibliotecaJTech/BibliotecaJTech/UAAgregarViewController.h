//
//  UAAgregarViewController.h
//  BibliotecaJTech
//
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UAAgregarViewController : UITableViewController<UISearchDisplayDelegate,NSURLConnectionDataDelegate, NSXMLParserDelegate>

@property(nonatomic,strong) NSMutableArray *libros;
@property(nonatomic,strong) NSArray *librosFiltrados;

@property(nonatomic,retain) NSURLConnection *currentConnection; //conexión
@property(nonatomic,retain) NSMutableData *downloadedData; //datos que se van descargando
@property(nonatomic,retain) NSMutableDictionary *downloadingImages; //imagenes descargadas

@end
