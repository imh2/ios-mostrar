//
//  Libro.h
//  BibliotecaJTech
//
//  Created by Lion User on 16/01/2013.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Libro : NSManagedObject

@property (nonatomic, retain) NSString * isbn;
@property (nonatomic, retain) NSString * titulo;
@property (nonatomic, retain) NSString * autor;
@property (nonatomic, retain) NSDate * fecha;
@property (nonatomic, retain) NSNumber * paginas;
@property (nonatomic, retain) NSString * imagen;
@property (nonatomic, retain) NSNumber * puntuacion;
@property (nonatomic, retain) NSNumber * estado;


@end
