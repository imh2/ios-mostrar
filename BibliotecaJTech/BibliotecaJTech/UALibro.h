//
//  Libro.h
//  Biblioteca


#import <Foundation/Foundation.h>

typedef enum {
    UAEstadoLibroSinLeer,
    UAEstadoLibroLeyendo,
    UAEstadoLibroLeido,    
} UAEstadoLibro;

@interface UALibro : NSObject

@property(nonatomic,strong) NSString *isbn;
@property(nonatomic,strong) NSString *titulo;
@property(nonatomic,strong) NSString *autor;
@property(nonatomic,strong) NSDate *fecha;
@property(nonatomic,assign) NSUInteger paginas;
@property(nonatomic,strong) NSString *imagen;
@property(nonatomic,assign) NSInteger puntuacion;
@property(nonatomic,assign) UAEstadoLibro estado;
@property (nonatomic, retain) UIImage * imagenPortada;

@end
