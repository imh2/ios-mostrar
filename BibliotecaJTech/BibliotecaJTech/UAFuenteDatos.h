//
//  FuenteDatos.h
//  Biblioteca
//


#import <Foundation/Foundation.h>

@class UALibro;

@interface UAFuenteDatos : NSObject

+ (id) sharedFuenteDatos;
- (NSArray *) buscaLibrosPorTitulo: (NSString *) titulo;
- (NSArray *) buscaLibrosPorIsbn: (NSString *) isbn;
- (BOOL) existeLibroEnColeccionConIsbn: (NSString*) isbn;
- (CGFloat) porcentajeLibrosLeidos;
- (CGFloat) notaMediaLibrosColeccion;
- (UALibro *) libroAleatorio;

@property (nonatomic, readonly, strong) NSMutableArray *librosBiblioteca; 
@property (nonatomic, readonly, strong) NSMutableArray *librosColeccion; 

@property (nonatomic, readonly, strong) NSArray *cadenaEstadoLibro;

@end
