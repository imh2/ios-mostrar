//
//  ViewController.m
//  EjercicioAgenda
// Copyright (c) 2013 jtech. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

NSString *contactName=nil;
NSString *contactLastname=nil;
NSString *address=nil;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tapSeleccionaContacto:(id)sender {
    
    // TODO: Muestra una ventana de selección de contactos
    
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;
    
    [self presentViewController:picker animated:YES completion:nil];
    
    
    
}

-(void) diHolaNombre:(NSString *)nombre yApellidos:(NSString *)apellidos
{
    // TODO: Mostrar alerta (UIAlertView) con un texto que muestre el nombre del contacto y sus apellidos
    
   NSString *cadena = [NSString stringWithFormat:@"%@/%@", nombre, apellidos];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Datos Contacto"
                                                    message:cadena
                                                   delegate:self
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"Ok", nil];
    
    [alert show];
}



-(void) muestraDireccionPostal:(NSString *)direccion
{
    NSLog(@"DIRECCION POSTAL: %@", direccion);
}


#pragma mark - ABPicker Delegate


#pragma mark - ABPicker Delegate
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (BOOL)peoplePickerNavigationController: (ABPeoplePickerNavigationController *)peoplePicker
      shouldContinueAfterSelectingPerson:(ABRecordRef)person {
    
    [self dismissViewControllerAnimated:YES completion:nil];


    
    // Obtenemos el nombre del contacto
    NSString* name = (__bridge_transfer NSString*) ABRecordCopyValue(person,
                                                                     kABPersonFirstNameProperty);
    NSString* surname = (__bridge_transfer NSString*) ABRecordCopyValue(person,
                                                                     kABPersonLastNameProperty);
    contactName=name;
    contactLastname=surname;
    
    [self diHolaNombre:name yApellidos:surname];
    
    //Direcciones postales del usuario
    NSDictionary* address = nil;
    ABMultiValueRef addresses = ABRecordCopyValue(person,
                                                  kABPersonAddressProperty);
    if (ABMultiValueGetCount(addresses) > 0) {
        address = (__bridge_transfer NSDictionary*) ABMultiValueCopyValueAtIndex(addresses, 0);
    } else {
        address = nil;
    }
    
    CFRelease(addresses);
    
    
    [self muestraDireccionPostal:[address description]];
    

    return NO;
}


- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
      shouldContinueAfterSelectingPerson:(ABRecordRef)person
                                property:(ABPropertyID)property
                              identifier:(ABMultiValueIdentifier)identifier
{
    return NO;
}

@end
