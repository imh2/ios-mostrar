//
//  ViewController.h
//  EjercicioAgenda
//Copyright (c) 2013 jtech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBookUI/AddressBookUI.h>

@interface ViewController : UIViewController <ABPeoplePickerNavigationControllerDelegate>
- (IBAction)tapSeleccionaContacto:(id)sender;

@end
