

#import <UIKit/UIKit.h>

@class User;

@interface ProfileViewController : UIViewController

@property (strong, nonatomic) User *user;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UILabel *currentLocation;

- (void) fillProfileViews;
- (void) editLocation;

@end
