Breves comentarios sobre los proyectos.

LevelSix:
	Proyecto construido completamente a mano -sin .xib ni storyboards - utilizando cocoapods y librería AFNetwork para conexiones con servicios -No esta adaptado a ios7- por lo que la barra de navegación de arriba oculta parcialmente parte del contenido, modelos de datos (usuario, photo, feeds …) para recoger y manejar datos en estructuras.


AlarmaJtech: Ejemplo de  utilización notificaciones con temporizacion, se introduce el texto y en cuantos segundos se quiere que aparezca la notificacion.

BibliotecaJtech: Proyecto completo , acceso a servicios REST, almacenamiento en base de datos con core data, interfaz gráfica con Storyboards, añadimos funcionalidad para publicar en twitter.
	La lista de libros carga del servicio, la colección propia es persistente en base de datos.


EjercicioAgenda: Ejemplo de acceso a los datos de contactos en el teléfono -hay que añadir contactos a la agenda del simulador para verlos-

Juego: Demo del SpaceAsteroids, para disparar pulsar de la mitad de la pantalla para arriba.

Procesamiento imagen: ejemplo de procesamiento de imagenes aplicando un filtro sepia. La de arriba emplea el motor de la CPU del teléfono. La imagen de abajo utiliza como motor la GPU.


El mas completo es el Proyecto BibliotecaJTech, pues es un ejemplo de aplicación completa y funcional donde hemos puesto en práctica los conocimientos adquiridos en ejercicios de toma de contacto como los que muestro en el resto de proyectos.